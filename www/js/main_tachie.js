

var hasClass = (function () {
    var div = document.createElement("div");
    if ("classList" in div && typeof div.classList.contains === "function") {
        return function (elem, className) {
            return elem.classList.contains(className);
        };
    } else {
        return function (elem, className) {
            var classes = elem.className.split(/\s+/);
            for (var i = 0; i < classes.length; i++) {
                if (classes[i] === className) {
                    return true;
                }
            }
            return false;
        };
    }
})();


function panelDisplay(activePanel, dcl) {
    // Do something...
    let tabLink = document.getElementById(dcl).querySelectorAll("a");
    for (let i = 0; i < tabLink.length; i++) {

        //設定條件tabLink ==activePanel 
        //將tablink代入for循環中並利用 if ...else 進行條件 classList.add增加class="active" ，就是每執行一次function的時候就進行全部tablinks增加class

        if (tabLink[i] == activePanel)
        // 假設目前的 tabLink 等於 activate, 改變他的class .active
        {
            tabLink[i].classList.add("fae");
            // 顯示面板的display:block 
            //  tabContents[i].style.display="block";

        } else {
            // 假設目前的 ablink 不等於 activate, 刪除他的class .active
            tabLink[i].classList.remove("fae");
            //隱藏 面板
            //  tabContents[i].style.display="none";
        }
    }
}



function panelDisplay2(activePanel, dcl) {
    // Do something...
    let tabLink = document.getElementById(dcl).querySelectorAll("div");
    for (let i = 0; i < tabLink.length; i++) {

        //設定條件tabLink ==activePanel 
        //將tablink代入for循環中並利用 if ...else 進行條件 classList.add增加class="active" ，就是每執行一次function的時候就進行全部tablinks增加class

        if (tabLink[i] == activePanel)
        // 假設目前的 tabLink 等於 activate, 改變他的class .active
        {
            tabLink[i].classList.add("clofsdf");

            // 顯示面板的display:block 
            //  tabContents[i].style.display="block";

        } else {
            // 假設目前的 ablink 不等於 activate, 刪除他的class .active
            tabLink[i].classList.remove("clofsdf");

            //隱藏 面板
            //  tabContents[i].style.display="none";
        }
    }
}
function panelDisplay2_eye(activePanel, dcl) {
    // Do something...
    let tabLink = document.getElementById(dcl).querySelectorAll("div");
    for (let i = 0; i < tabLink.length; i++) {

        //設定條件tabLink ==activePanel 
        //將tablink代入for循環中並利用 if ...else 進行條件 classList.add增加class="active" ，就是每執行一次function的時候就進行全部tablinks增加class

        if (tabLink[i] == activePanel)
        // 假設目前的 tabLink 等於 activate, 改變他的class .active
        {
            tabLink[i].classList.add("clofsdf_eye");
            // 顯示面板的display:block 
            //  tabContents[i].style.display="block";

        } else {
            // 假設目前的 ablink 不等於 activate, 刪除他的class .active

            tabLink[i].classList.remove("clofsdf_eye");
            //隱藏 面板
            //  tabContents[i].style.display="none";
        }
    }
}

//素體動態
function addTBody(e) {	//刪除末列

    panelDisplay(e, "selert-body_ts");
    document.getElementById('body_chid_ts').src = './assets/tachie/body/' + e.getAttribute("imgname");

    localStorage.setItem("RST_body", e.getAttribute("imgname"));
    var img_url = './assets/tachie/body/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印





        var t = document.getElementById('body_img2_ts'),
            s = document.getElementById('body_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';






    }
}

//前髮動態
function addTFront_hairs(e) {	//刪除末列

    panelDisplay(e, "selert-front_hairs_ts");
    document.getElementById('front_hairs_chid_ts').src = './assets/tachie/front_hairs/' + e.getAttribute("imgname");

    localStorage.setItem("RST_front_hairs", e.getAttribute("imgname"));
    var img_url = './assets/tachie/front_hairs/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印





        var t = document.getElementById('front_hairs_img2_ts'),
            s = document.getElementById('front_hairs_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';






    }
}
//後髮動態
function addTBack_hairs(e) {	//刪除末列

    panelDisplay(e, "selert-back_hairs_ts");
    document.getElementById('back_hairs_chid_ts').src = './assets/tachie/back_hairs/' + e.getAttribute("imgname");

    localStorage.setItem("RST_back_hairs", e.getAttribute("imgname"));
    var img_url = './assets/tachie/back_hairs/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印





        var t = document.getElementById('back_hairs_img2_ts'),
            s = document.getElementById('back_hairs_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';






    }
}

//側髮動態
function addTSide_hairs(e) {	//刪除末列

    panelDisplay(e, "selert-side_hairs_ts");
    document.getElementById('side_hairs_chid_ts').src = './assets/tachie/side_hairs/' + e.getAttribute("imgname");

    localStorage.setItem("RST_side_hairs", e.getAttribute("imgname"));
    var img_url = './assets/tachie/side_hairs/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印





        var t = document.getElementById('side_hairs_img2_ts'),
            s = document.getElementById('side_hairs_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';






    }
}
//眼睛動態
function addTEyes(e) {	//刪除末列

    panelDisplay(e, "selert-eyes_ts");
    document.getElementById('eyes_chid_ts').src = './assets/tachie/eyes/' + e.getAttribute("imgname");

    localStorage.setItem("RST_eyes", e.getAttribute("imgname"));
    var img_url = './assets/tachie/eyes/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印





        var t = document.getElementById('eyes_img2_ts'),
            s = document.getElementById('eyes_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';






    }
}

//眉毛動態
function addTEyebrow(e) {	//刪除末列

    panelDisplay(e, "selert-eyebrow_ts");
    document.getElementById('eyebrow_chid_ts').src = './assets/tachie/eyebrow/' + e.getAttribute("imgname");

    localStorage.setItem("RST_eyebrow", e.getAttribute("imgname"));
    var img_url = './assets/tachie/eyebrow/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印





        var t = document.getElementById('eyebrow_img2_ts'),
            s = document.getElementById('eyebrow_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';






    }
}

//嘴巴動態
function addTMouth(e) {	//刪除末列

    panelDisplay(e, "selert-mouth_ts");
    document.getElementById('mouth_chid_ts').src = './assets/tachie/mouth/' + e.getAttribute("imgname");

    localStorage.setItem("RST_mouth", e.getAttribute("imgname"));
    var img_url = './assets/tachie/mouth/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印





        var t = document.getElementById('mouth_img2_ts'),
            s = document.getElementById('mouth_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';






    }
}


//套裝動態
function addTCoat(e) {	//刪除末列

    panelDisplay(e, "selert-coat_ts");
    document.getElementById('coat_chid_ts').src = './assets/tachie/coat/' + e.getAttribute("imgname");

    localStorage.setItem("RST_coat", e.getAttribute("imgname"));
    var img_url = './assets/tachie/coat/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印





        var t = document.getElementById('coat_img2_ts'),
            s = document.getElementById('coat_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';






    }
}



//臉飾動態
function addTFace_decoration(e) {	//刪除末列

    panelDisplay(e, "selert-face_decoration_ts");
    if (e.getAttribute("imgname") == 'None_ts.png') {
        document.getElementById('face_decoration_chid_ts').src = './img/None_ts.png';
    } else {
        document.getElementById('face_decoration_chid_ts').src = './assets/tachie/face_decoration/' + e.getAttribute("imgname");
    }





    localStorage.setItem("RST_face_decoration", e.getAttribute("imgname"));
    var img_url = e.getAttribute("imgname") == 'None_ts.png' ? './img/None_ts.png' : './assets/tachie/face_decoration/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印





        var t = document.getElementById('face_decoration_img2_ts'),
            s = document.getElementById('face_decoration_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';






    }
}

//眼鏡動態
function addTGlasses(e) {	//刪除末列

    panelDisplay(e, "selert-glasses_ts");
    if (e.getAttribute("imgname") == 'None_ts.png') {
        document.getElementById('glasses_chid_ts').src = './img/None_ts.png';
    } else {
        document.getElementById('glasses_chid_ts').src = './assets/tachie/glasses/' + e.getAttribute("imgname");
    }





    localStorage.setItem("RST_glasses", e.getAttribute("imgname"));
    var img_url = e.getAttribute("imgname") == 'None_ts.png' ? './img/None_ts.png' : './assets/tachie/glasses/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印





        var t = document.getElementById('glasses_img2_ts'),
            s = document.getElementById('glasses_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';






    }
}

//呆毛動態
function addTAhoge(e) {	//刪除末列

    panelDisplay(e, "selert-ahoge_ts");
    if (e.getAttribute("imgname") == 'None_ts.png') {
        document.getElementById('ahoge_chid_ts').src = './img/None_ts.png';
    } else {
        document.getElementById('ahoge_chid_ts').src = './assets/tachie/ahoge/' + e.getAttribute("imgname");
    }





    localStorage.setItem("RST_ahoge", e.getAttribute("imgname"));
    var img_url = e.getAttribute("imgname") == 'None_ts.png' ? './img/None_ts.png' : './assets/tachie/ahoge/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印





        var t = document.getElementById('ahoge_img2_ts'),
            s = document.getElementById('ahoge_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';






    }
}


//頸飾動態
function addTNeck_accessories(e) {	//刪除末列

    panelDisplay(e, "selert-neck_accessories_ts");
    if (e.getAttribute("imgname") == 'None_ts.png') {
        document.getElementById('neck_accessories_chid_ts').src = './img/None_ts.png';
    } else {
        document.getElementById('neck_accessories_chid_ts').src = './assets/tachie/neck_accessories/' + e.getAttribute("imgname");
    }





    localStorage.setItem("RST_neck_accessories", e.getAttribute("imgname"));
    var img_url = e.getAttribute("imgname") == 'None_ts.png' ? './img/None_ts.png' : './assets/tachie/neck_accessories/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印





        var t = document.getElementById('neck_accessories_img2_ts'),
            s = document.getElementById('neck_accessories_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';






    }
}

//帽子動態
function addTHats(e) {	//刪除末列

    panelDisplay(e, "selert-hats_ts");
    if (e.getAttribute("imgname") == 'None_ts.png') {
        document.getElementById('hats_chid_ts').src = './img/None_ts.png';
    } else {
        document.getElementById('hats_chid_ts').src = './assets/tachie/hats/' + e.getAttribute("imgname");
    }

    localStorage.setItem("RST_hats", e.getAttribute("imgname"));
    var img_url = e.getAttribute("imgname") == 'None_ts.png' ? './img/None_ts.png' : './assets/tachie/hats/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

        var t = document.getElementById('hats_img2_ts'),
            s = document.getElementById('hats_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';

    }
}

//頭飾動態
function addTHeadwear(e) {	//刪除末列

    panelDisplay(e, "selert-headwear_ts");
    if (e.getAttribute("imgname") == 'None_ts.png') {
        document.getElementById('headwear_chid_ts').src = './img/None_ts.png';
    } else {
        document.getElementById('headwear_chid_ts').src = './assets/tachie/headwear/' + e.getAttribute("imgname");
    }

    localStorage.setItem("RST_headwear", e.getAttribute("imgname"));
    var img_url = e.getAttribute("imgname") == 'None_ts.png' ? './img/None_ts.png' : './assets/tachie/headwear/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

        var t = document.getElementById('headwear_img2_ts'),
            s = document.getElementById('headwear_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';

    }
}

//上衣動態
function addTJacket(e) {	//刪除末列

    panelDisplay(e, "selert-jacket_ts");
    if (e.getAttribute("imgname") == 'None_ts.png') {
        document.getElementById('jacket_chid_ts').src = './img/None_ts.png';
    } else {
        document.getElementById('jacket_chid_ts').src = './assets/tachie/jacket/' + e.getAttribute("imgname");
    }

    localStorage.setItem("RST_jacket", e.getAttribute("imgname"));
    var img_url = e.getAttribute("imgname") == 'None_ts.png' ? './img/None_ts.png' : './assets/tachie/jacket/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

        var t = document.getElementById('jacket_img2_ts'),
            s = document.getElementById('jacket_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';

    }
}

//裙子褲子動態
function addTSkirt_pants(e) {	//刪除末列

    panelDisplay(e, "selert-skirt_pants_ts");
    if (e.getAttribute("imgname") == 'None_ts.png') {
        document.getElementById('skirt_pants_chid_ts').src = './img/None_ts.png';
    } else {
        document.getElementById('skirt_pants_chid_ts').src = './assets/tachie/skirt_pants/' + e.getAttribute("imgname");
    }

    localStorage.setItem("RST_skirt_pants", e.getAttribute("imgname"));
    var img_url = e.getAttribute("imgname") == 'None_ts.png' ? './img/None_ts.png' : './assets/tachie/skirt_pants/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

        var t = document.getElementById('skirt_pants_img2_ts'),
            s = document.getElementById('skirt_pants_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';

    }
}


//襪子動態
function addTSock(e) {	//刪除末列

    panelDisplay(e, "selert-sock_ts");
    if (e.getAttribute("imgname") == 'None_ts.png') {
        document.getElementById('sock_chid_ts').src = './img/None_ts.png';
    } else {
        document.getElementById('sock_chid_ts').src = './assets/tachie/sock/' + e.getAttribute("imgname");
    }

    localStorage.setItem("RST_sock", e.getAttribute("imgname"));
    var img_url = e.getAttribute("imgname") == 'None_ts.png' ? './img/None_ts.png' : './assets/tachie/sock/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

        var t = document.getElementById('sock_img2_ts'),
            s = document.getElementById('sock_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';

    }
}


//鞋子動態
function addTShoes(e) {	//刪除末列

    panelDisplay(e, "selert-shoes_ts");
    if (e.getAttribute("imgname") == 'None_ts.png') {
        document.getElementById('shoes_chid_ts').src = './img/None_ts.png';
    } else {
        document.getElementById('shoes_chid_ts').src = './assets/tachie/shoes/' + e.getAttribute("imgname");
    }

    localStorage.setItem("RST_shoes", e.getAttribute("imgname"));
    var img_url = e.getAttribute("imgname") == 'None_ts.png' ? './img/None_ts.png' : './assets/tachie/shoes/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

        var t = document.getElementById('shoes_img2_ts'),
            s = document.getElementById('shoes_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';

    }
}

//尾巴動態
function addTTail(e) {	//刪除末列

    panelDisplay(e, "selert-tail_ts");
    if (e.getAttribute("imgname") == 'None_ts.png') {
        document.getElementById('tail_chid_ts').src = './img/None_ts.png';
    } else {
        document.getElementById('tail_chid_ts').src = './assets/tachie/tail/' + e.getAttribute("imgname");
    }

    localStorage.setItem("RST_tail", e.getAttribute("imgname"));
    var img_url = e.getAttribute("imgname") == 'None_ts.png' ? './img/None_ts.png' : './assets/tachie/tail/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

        var t = document.getElementById('tail_img2_ts'),
            s = document.getElementById('tail_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';

    }
}

//其他動態
function addTOthers(e) {	//刪除末列

    panelDisplay(e, "selert-others_ts");
    if (e.getAttribute("imgname") == 'None_ts.png') {
        document.getElementById('others_chid_ts').src = './img/None_ts.png';
    } else {
        document.getElementById('others_chid_ts').src = './assets/tachie/others/' + e.getAttribute("imgname");
    }

    localStorage.setItem("RST_others", e.getAttribute("imgname"));
    var img_url = e.getAttribute("imgname") == 'None_ts.png' ? './img/None_ts.png' : './assets/tachie/others/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

        var t = document.getElementById('others_img2_ts'),
            s = document.getElementById('others_chid_ts').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';

    }
}




document.addEventListener("DOMContentLoaded", function () {

    //讀取目錄
    var fs = require('fs');
    var asset_basic_hairs_ts = [];
    var asset_body_ts = [];
    var asset_front_hairs_ts = [];
    var asset_back_hairs_ts = [];
    var asset_side_hairs_ts = [];
    var asset_eyes_ts = [];
    var asset_eyebrow_ts = [];
    var asset_mouth_ts = [];
    var asset_coat_ts = [];
    var asset_face_decoration_ts = [];
    var asset_glasses_ts = [];
    var asset_ahoge_ts = [];
    var asset_neck_accessories_ts = [];
    var asset_hats_ts = [];
    var asset_headwear_ts = [];
    var asset_jacket_ts = [];
    var asset_skirt_pants_ts = [];
    var asset_sock_ts = [];
    var asset_shoes_ts = [];
    var asset_tail_ts = [];
    var asset_others_ts = [];

    let Tachie_comAsset = [
        "back_hairs",
        "tail",
        "body",
        "basic_hairs",
        "face_decoration",
        "eyes",
        "glasses",
        "mouth",
        "sock",
        "coat",
        "skirt_pants",
        "jacket",
        "side_hairs",
        "front_hairs",
        "eyebrow",
        "headwear",
        "ahoge",
        "hats",
        "neck_accessories",
        "shoes",
        "others"

    ];






    /*  let canvas = document.createElement('canvas');
     let context = canvas.getContext('2d'); */

    //let base64s = "";

    /* for (let i = 0; i < Tachie_comAsset.length; i++) {

        localStorage.setItem("RST_" + Tachie_comAsset[i], null);
        list_Asset[Tachie_comAsset[i]] = [];
        divAssetTab += `<a class="nav-link" id="v-pills-` + Tachie_comAsset[i] + `-tab" data-toggle="pill" href="#` + Tachie_comAsset[i] + `_TS" role="tab"
    aria-controls="v-pills-`+ Tachie_comAsset[i] + `" aria-selected="false" data-tab="` + Tachie_comAsset[i] + `">` + Tachie_comAsset[i] + `</a>
    `;
        divAssetCon += `<div class="tab-pane fade" id="` + Tachie_comAsset[i] + `_TS" role="tabpanel" aria-labelledby="v-pills-` + Tachie_comAsset[i] + `-tab">
    ...</div>
    `



    }
    console.log(files_ts) 
    document.getElementById('menu').innerHTML = divAssetTab;
    document.getElementById('avatar').innerHTML = divAssetCon;
*/
    let fsAssetConS;

    let list_Asset = {};
    let head = document.head || document.getElementsByTagName('head')[0];
    for (let i = 0; i < Tachie_comAsset.length; i++) {




        fs.readdir('./www/assets/tachie/' + Tachie_comAsset[i] + '/', function (err, files) {
            if (err) {
                return console.error(err);
            }
            files.forEach(function (file) {
                list_Asset[Tachie_comAsset[i]].push(file);

                for (let r = 0; r < list_Asset[Tachie_comAsset[i]].length; r++) {
                    // console.log(list_Asset[Tachie_comAsset[0]][r])
                    fsAssetConS = document.createElement('link');

                    /*   bgImg[list_Asset[Tachie_comAsset[0]][r]].onload = function () {
  
                          canvas.width = 400;
                          canvas.height = 300;
                          context.drawImage(bgImg[list_Asset[Tachie_comAsset[0]][r]], 0, 0, 400, 300);
                          base64s = canvas.toDataURL('image/png', 1.0);
  
                          fsAssetCon += `<img class="img-thumbnail" src="` + base64s + `"/>`
                          if (list_Asset[Tachie_comAsset[i]].length) {
  
  
  
                              document.getElementById(Tachie_comAsset[i]).innerHTML = fsAssetCon;
                              base64s = "";
                              fsAssetCon = "";
                          }
                      }; */


                    fsAssetConS.setAttribute("rel", "prefetch");
                    fsAssetConS.setAttribute("href", "./assets/tachie/" + Tachie_comAsset[i] + "/" + list_Asset[Tachie_comAsset[i]][r]);
                    head.appendChild(fsAssetConS);

                };


            });


        });
    };
    let fsAssetConS1a = document.createElement('link');
    let fsAssetConS2a = document.createElement('link');
    let fsAssetConS3a = document.createElement('link');

    fsAssetConS1a.setAttribute("rel", "prefetch");
    fsAssetConS1a.setAttribute("href", "./img/neko_bg.png");
    head.appendChild(fsAssetConS1a);
    fsAssetConS2a.setAttribute("rel", "prefetch");
    fsAssetConS2a.setAttribute("href", "./img/None_ts.png");
    head.appendChild(fsAssetConS2a);
    fsAssetConS3a.setAttribute("rel", "prefetch");
    fsAssetConS3a.setAttribute("href", "./img/None.png");
    head.appendChild(fsAssetConS3a);

    //如果路徑指定不規範，則會按當前檔案所在目錄處理
    //如果為目錄不存在丟擲異常 { [Error: ENOENT, scandir 'F:\test1'] errno: -4058, code: 'ENOENT', path: 'F:\\test1' }


    localStorage.setItem("WST_width", null);
    localStorage.setItem("WST_height", null);

    localStorage.setItem("RST_basic_hairs", null);
    localStorage.setItem("RST_body", null);
    localStorage.setItem("RST_front_hairs", null);
    localStorage.setItem("RST_back_hairs", null);
    localStorage.setItem("RST_side_hairs", null);
    localStorage.setItem("RST_eyes", null);
    localStorage.setItem("RST_eyebrow", null);
    localStorage.setItem("RST_mouth", null);
    localStorage.setItem("RST_coat", null);
    localStorage.setItem("RST_face_decoration", null);
    localStorage.setItem("RST_glasses", null);
    localStorage.setItem("RST_ahoge", null);
    localStorage.setItem("RST_neck_accessories", null);
    localStorage.setItem("RST_hats", null);
    localStorage.setItem("RST_headwear", null);
    localStorage.setItem("RST_jacket", null);
    localStorage.setItem("RST_skirt_pants", null);
    localStorage.setItem("RST_sock", null);
    localStorage.setItem("RST_shoes", null);
    localStorage.setItem("RST_tail", null);
    localStorage.setItem("RST_others", null);

    localStorage.setItem("RST_basic_hairs_SD", null);
    localStorage.setItem("RST_body_SD", null);
    localStorage.setItem("RST_front_hairs_SD", null);
    localStorage.setItem("RST_back_hairs_SD", null);
    localStorage.setItem("RST_side_hairs_SD", null);
    localStorage.setItem("RST_eyes_SD", null);
    localStorage.setItem("RST_eyebrow_SD", null);
    localStorage.setItem("RST_mouth_SD", null);
    localStorage.setItem("RST_coat_SD", null);



    //基礎頭髮


    fs.readdir('./www/assets/tachie/basic_hairs/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_basic_hairs_ts.push(file);

        });

        document.getElementById('basic_hairs_chid_ts').src = './assets/tachie/basic_hairs/' + asset_basic_hairs_ts[0];

        localStorage.setItem("RST_basic_hairs_SD", asset_basic_hairs_ts[0]);






        var img_url = './assets/tachie/basic_hairs/' + asset_basic_hairs_ts[0];


        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印
            localStorage.setItem("WST_width", img.width);
            localStorage.setItem("WST_height", img.height);
            localStorage.setItem("RST_basic_hairs", asset_basic_hairs_ts[0]);

            var t = document.getElementById('basic_hairs_img2_ts'),
                s = document.getElementById('basic_hairs_chid_ts').getAttribute('src');

            t.style.backgroundImage = 'url(' + s + ')';

        }

    });



    //素體


    fs.readdir('./www/assets/tachie/body/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_body_ts.push(file);

        });

        document.getElementById('body_chid_ts').src = './assets/tachie/body/' + asset_body_ts[0];

        localStorage.setItem("RST_body_SD", asset_body_ts[0]);



        for (var i = 0; i < asset_body_ts.length; i++) {
            // document.getElementById('pills-body').appendChild

            var ul = document.getElementById('selert-body_ts');
            //新增 li
            var li = document.createElement("a");
            //新增 img
            var img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Tbody_" + i);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTBody(this)");
            li.setAttribute("imgname", asset_body_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/body/' + asset_body_ts[i];
            li.appendChild(img);
            ul.appendChild(li);






        }

        document.getElementById('Tbody_0').classList.add("fae");


        var img_url = './assets/tachie/body/' + asset_body_ts[0];


        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印
            localStorage.setItem("WST_width", img.width);
            localStorage.setItem("WST_height", img.height);
            localStorage.setItem("RST_body", asset_body_ts[0]);

            var t = document.getElementById('body_img2_ts'),
                s = document.getElementById('body_chid_ts').getAttribute('src');

            t.style.backgroundImage = 'url(' + s + ')';

        }

    });


    //前髮


    fs.readdir('./www/assets/tachie/front_hairs/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_front_hairs_ts.push(file);

        });

        document.getElementById('front_hairs_chid_ts').src = './assets/tachie/front_hairs/' + asset_front_hairs_ts[0];

        localStorage.setItem("RST_front_hairs_SD", asset_front_hairs_ts[0]);

        for (var i = 0; i < asset_front_hairs_ts.length; i++) {


            var ul = document.getElementById('selert-front_hairs_ts');
            //新增 li
            var li = document.createElement("a");
            //新增 img
            var img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Tfront_hairs_" + i);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTFront_hairs(this)");
            li.setAttribute("imgname", asset_front_hairs_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/front_hairs/' + asset_front_hairs_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Tfront_hairs_0').classList.add("fae");
        var img_url = './assets/tachie/front_hairs/' + asset_front_hairs_ts[0];
        var img = new Image()
        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印
            localStorage.setItem("WST_width", img.width);
            localStorage.setItem("WST_height", img.height);
            localStorage.setItem("RST_front_hairs", asset_front_hairs_ts[0]);

            var t = document.getElementById('front_hairs_img2_ts'),
                s = document.getElementById('front_hairs_chid_ts').getAttribute('src');

            t.style.backgroundImage = 'url(' + s + ')';

        }

    });



    //後髮


    fs.readdir('./www/assets/tachie/back_hairs/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_back_hairs_ts.push(file);

        });

        document.getElementById('back_hairs_chid_ts').src = './assets/tachie/back_hairs/' + asset_back_hairs_ts[0];

        localStorage.setItem("RST_back_hairs_SD", asset_back_hairs_ts[0]);

        for (var i = 0; i < asset_back_hairs_ts.length; i++) {


            var ul = document.getElementById('selert-back_hairs_ts');
            //新增 li
            var li = document.createElement("a");
            //新增 img
            var img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Tback_hairs_" + i);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTBack_hairs(this)");
            li.setAttribute("imgname", asset_back_hairs_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/back_hairs/' + asset_back_hairs_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Tback_hairs_0').classList.add("fae");
        var img_url = './assets/tachie/back_hairs/' + asset_back_hairs_ts[0];
        var img = new Image()
        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印
            localStorage.setItem("WST_width", img.width);
            localStorage.setItem("WST_height", img.height);
            localStorage.setItem("RST_back_hairs", asset_back_hairs_ts[0]);

            var t = document.getElementById('back_hairs_img2_ts'),
                s = document.getElementById('back_hairs_chid_ts').getAttribute('src');

            t.style.backgroundImage = 'url(' + s + ')';

        }

    });

    //側髮


    fs.readdir('./www/assets/tachie/side_hairs/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_side_hairs_ts.push(file);

        });

        document.getElementById('side_hairs_chid_ts').src = './assets/tachie/side_hairs/' + asset_side_hairs_ts[0];

        localStorage.setItem("RST_side_hairs_SD", asset_side_hairs_ts[0]);

        for (var i = 0; i < asset_side_hairs_ts.length; i++) {


            var ul = document.getElementById('selert-side_hairs_ts');
            //新增 li
            var li = document.createElement("a");
            //新增 img
            var img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Tside_hairs_" + i);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTSide_hairs(this)");
            li.setAttribute("imgname", asset_side_hairs_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/side_hairs/' + asset_side_hairs_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Tside_hairs_0').classList.add("fae");
        var img_url = './assets/tachie/side_hairs/' + asset_side_hairs_ts[0];
        var img = new Image()
        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印
            localStorage.setItem("WST_width", img.width);
            localStorage.setItem("WST_height", img.height);
            localStorage.setItem("RST_side_hairs", asset_side_hairs_ts[0]);

            var t = document.getElementById('side_hairs_img2_ts'),
                s = document.getElementById('side_hairs_chid_ts').getAttribute('src');

            t.style.backgroundImage = 'url(' + s + ')';

        }

    });

    //眼睛


    fs.readdir('./www/assets/tachie/eyes/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_eyes_ts.push(file);

        });

        document.getElementById('eyes_chid_ts').src = './assets/tachie/eyes/' + asset_eyes_ts[0];

        localStorage.setItem("RST_eyes_SD", asset_eyes_ts[0]);

        for (var i = 0; i < asset_eyes_ts.length; i++) {


            var ul = document.getElementById('selert-eyes_ts');
            //新增 li
            var li = document.createElement("a");
            //新增 img
            var img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Teyes_" + i);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTEyes(this)");
            li.setAttribute("imgname", asset_eyes_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/eyes/' + asset_eyes_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Teyes_0').classList.add("fae");
        var img_url = './assets/tachie/eyes/' + asset_eyes_ts[0];
        var img = new Image()
        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印
            localStorage.setItem("WST_width", img.width);
            localStorage.setItem("WST_height", img.height);
            localStorage.setItem("RST_eyes", asset_eyes_ts[0]);

            var t = document.getElementById('eyes_img2_ts'),
                s = document.getElementById('eyes_chid_ts').getAttribute('src');

            t.style.backgroundImage = 'url(' + s + ')';

        }

    });


    //眉毛


    fs.readdir('./www/assets/tachie/eyebrow/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_eyebrow_ts.push(file);

        });

        document.getElementById('eyebrow_chid_ts').src = './assets/tachie/eyebrow/' + asset_eyebrow_ts[0];

        localStorage.setItem("RST_eyebrow_SD", asset_eyebrow_ts[0]);

        for (var i = 0; i < asset_eyebrow_ts.length; i++) {


            var ul = document.getElementById('selert-eyebrow_ts');
            //新增 li
            var li = document.createElement("a");
            //新增 img
            var img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Teyebrow_" + i);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTEyebrow(this)");
            li.setAttribute("imgname", asset_eyebrow_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/eyebrow/' + asset_eyebrow_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Teyebrow_0').classList.add("fae");
        var img_url = './assets/tachie/eyebrow/' + asset_eyebrow_ts[0];
        var img = new Image()
        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印
            localStorage.setItem("WST_width", img.width);
            localStorage.setItem("WST_height", img.height);
            localStorage.setItem("RST_eyebrow", asset_eyebrow_ts[0]);

            var t = document.getElementById('eyebrow_img2_ts'),
                s = document.getElementById('eyebrow_chid_ts').getAttribute('src');

            t.style.backgroundImage = 'url(' + s + ')';

        }

    });

    //嘴巴


    fs.readdir('./www/assets/tachie/mouth/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_mouth_ts.push(file);

        });

        document.getElementById('mouth_chid_ts').src = './assets/tachie/mouth/' + asset_mouth_ts[0];

        localStorage.setItem("RST_mouth_SD", asset_mouth_ts[0]);

        for (var i = 0; i < asset_mouth_ts.length; i++) {


            var ul = document.getElementById('selert-mouth_ts');
            //新增 li
            var li = document.createElement("a");
            //新增 img
            var img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Tmouth_" + i);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTMouth(this)");
            li.setAttribute("imgname", asset_mouth_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/mouth/' + asset_mouth_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Tmouth_0').classList.add("fae");
        var img_url = './assets/tachie/mouth/' + asset_mouth_ts[0];
        var img = new Image()
        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印
            localStorage.setItem("WST_width", img.width);
            localStorage.setItem("WST_height", img.height);
            localStorage.setItem("RST_mouth", asset_mouth_ts[0]);

            var t = document.getElementById('mouth_img2_ts'),
                s = document.getElementById('mouth_chid_ts').getAttribute('src');

            t.style.backgroundImage = 'url(' + s + ')';

        }

    });



    //套裝


    fs.readdir('./www/assets/tachie/coat/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_coat_ts.push(file);

        });

        document.getElementById('coat_chid_ts').src = './assets/tachie/coat/' + asset_coat_ts[0];

        localStorage.setItem("RST_coat_SD", asset_coat_ts[0]);

        for (var i = 0; i < asset_coat_ts.length; i++) {


            var ul = document.getElementById('selert-coat_ts');
            //新增 li
            var li = document.createElement("a");
            //新增 img
            var img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Tcoat_" + i);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTCoat(this)");
            li.setAttribute("imgname", asset_coat_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/coat/' + asset_coat_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Tcoat_0').classList.add("fae");
        var img_url = './assets/tachie/coat/' + asset_coat_ts[0];
        var img = new Image()
        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印
            localStorage.setItem("WST_width", img.width);
            localStorage.setItem("WST_height", img.height);
            localStorage.setItem("RST_coat", asset_coat_ts[0]);

            var t = document.getElementById('coat_img2_ts'),
                s = document.getElementById('coat_chid_ts').getAttribute('src');

            t.style.backgroundImage = 'url(' + s + ')';

        }

    });




    //臉飾
    fs.readdir('./www/assets/tachie/face_decoration/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_face_decoration_ts.push(file);

        });

        document.getElementById('face_decoration_chid_ts').src = './img/None_ts.png';

        var ul = document.getElementById('selert-face_decoration_ts');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "Tface_decoration_0");
        li.setAttribute("class", "boty_igm_ts");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addTFace_decoration(this)");
        li.setAttribute("imgname", "None_ts.png");
        //設定 img 圖片地址
        img.src = './img/None_ts.png';
        li.appendChild(img);
        ul.appendChild(li);

        for (var i = 0; i < asset_face_decoration_ts.length; i++) {

            ul = document.getElementById('selert-face_decoration_ts');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Tface_decoration_" + i + 1);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTFace_decoration(this)");
            li.setAttribute("imgname", asset_face_decoration_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/face_decoration/' + asset_face_decoration_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Tface_decoration_0').classList.add("fae");
        var img_url = './img/None_ts.png';
        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印

            localStorage.setItem("RST_face_decoration", "None_ts.png");
            var t = document.getElementById('face_decoration_img2_ts'),
                s = document.getElementById('face_decoration_chid_ts').getAttribute('src');
            t.style.backgroundImage = 'url(' + s + ')';

        }

    });



    //眼鏡
    fs.readdir('./www/assets/tachie/glasses/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_glasses_ts.push(file);

        });

        document.getElementById('glasses_chid_ts').src = './img/None_ts.png';

        var ul = document.getElementById('selert-glasses_ts');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "Tglasses_0");
        li.setAttribute("class", "boty_igm_ts");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addTGlasses(this)");
        li.setAttribute("imgname", "None_ts.png");
        //設定 img 圖片地址
        img.src = './img/None_ts.png';
        li.appendChild(img);
        ul.appendChild(li);

        for (var i = 0; i < asset_glasses_ts.length; i++) {

            ul = document.getElementById('selert-glasses_ts');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Tglasses_" + i + 1);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTGlasses(this)");
            li.setAttribute("imgname", asset_glasses_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/glasses/' + asset_glasses_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Tglasses_0').classList.add("fae");
        var img_url = './img/None_ts.png';
        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印

            localStorage.setItem("RST_glasses", "None_ts.png");
            var t = document.getElementById('glasses_img2_ts'),
                s = document.getElementById('glasses_chid_ts').getAttribute('src');
            t.style.backgroundImage = 'url(' + s + ')';

        }

    });


    //呆毛
    fs.readdir('./www/assets/tachie/ahoge/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_ahoge_ts.push(file);

        });

        document.getElementById('ahoge_chid_ts').src = './img/None_ts.png';

        var ul = document.getElementById('selert-ahoge_ts');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "Tahoge_0");
        li.setAttribute("class", "boty_igm_ts");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addTAhoge(this)");
        li.setAttribute("imgname", "None_ts.png");
        //設定 img 圖片地址
        img.src = './img/None_ts.png';
        li.appendChild(img);
        ul.appendChild(li);

        for (var i = 0; i < asset_ahoge_ts.length; i++) {

            ul = document.getElementById('selert-ahoge_ts');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Tahoge_" + i + 1);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTAhoge(this)");
            li.setAttribute("imgname", asset_ahoge_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/ahoge/' + asset_ahoge_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Tahoge_0').classList.add("fae");
        var img_url = './img/None_ts.png';
        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印

            localStorage.setItem("RST_ahoge", "None_ts.png");
            var t = document.getElementById('ahoge_img2_ts'),
                s = document.getElementById('ahoge_chid_ts').getAttribute('src');
            t.style.backgroundImage = 'url(' + s + ')';

        }

    });



    //呆毛
    fs.readdir('./www/assets/tachie/ahoge/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_ahoge_ts.push(file);

        });

        document.getElementById('ahoge_chid_ts').src = './img/None_ts.png';

        var ul = document.getElementById('selert-ahoge_ts');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "Tahoge_0");
        li.setAttribute("class", "boty_igm_ts");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addTAhoge(this)");
        li.setAttribute("imgname", "None_ts.png");
        //設定 img 圖片地址
        img.src = './img/None_ts.png';
        li.appendChild(img);
        ul.appendChild(li);

        for (var i = 0; i < asset_ahoge_ts.length; i++) {

            ul = document.getElementById('selert-ahoge_ts');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Tahoge_" + i + 1);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTAhoge(this)");
            li.setAttribute("imgname", asset_ahoge_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/ahoge/' + asset_ahoge_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Tahoge_0').classList.add("fae");
        var img_url = './img/None_ts.png';
        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印

            localStorage.setItem("RST_ahoge", "None_ts.png");
            var t = document.getElementById('ahoge_img2_ts'),
                s = document.getElementById('ahoge_chid_ts').getAttribute('src');
            t.style.backgroundImage = 'url(' + s + ')';

        }

    });


    //頸飾
    fs.readdir('./www/assets/tachie/neck_accessories/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_neck_accessories_ts.push(file);

        });

        document.getElementById('neck_accessories_chid_ts').src = './img/None_ts.png';

        var ul = document.getElementById('selert-neck_accessories_ts');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "Tneck_accessories_0");
        li.setAttribute("class", "boty_igm_ts");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addTNeck_accessories(this)");
        li.setAttribute("imgname", "None_ts.png");
        //設定 img 圖片地址
        img.src = './img/None_ts.png';
        li.appendChild(img);
        ul.appendChild(li);

        for (var i = 0; i < asset_neck_accessories_ts.length; i++) {

            ul = document.getElementById('selert-neck_accessories_ts');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Tneck_accessories_" + i + 1);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTNeck_accessories(this)");
            li.setAttribute("imgname", asset_neck_accessories_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/neck_accessories/' + asset_neck_accessories_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Tneck_accessories_0').classList.add("fae");
        var img_url = './img/None_ts.png';
        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印

            localStorage.setItem("RST_neck_accessories", "None_ts.png");
            var t = document.getElementById('neck_accessories_img2_ts'),
                s = document.getElementById('neck_accessories_chid_ts').getAttribute('src');
            t.style.backgroundImage = 'url(' + s + ')';

        }

    });

    //帽子
    fs.readdir('./www/assets/tachie/hats/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_hats_ts.push(file);

        });

        document.getElementById('hats_chid_ts').src = './img/None_ts.png';

        var ul = document.getElementById('selert-hats_ts');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "Thats_0");
        li.setAttribute("class", "boty_igm_ts");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addTHats(this)");
        li.setAttribute("imgname", "None_ts.png");
        //設定 img 圖片地址
        img.src = './img/None_ts.png';
        li.appendChild(img);
        ul.appendChild(li);

        for (var i = 0; i < asset_hats_ts.length; i++) {

            ul = document.getElementById('selert-hats_ts');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Thats_" + i + 1);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTHats(this)");
            li.setAttribute("imgname", asset_hats_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/hats/' + asset_hats_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Thats_0').classList.add("fae");
        var img_url = './img/None_ts.png';
        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印

            localStorage.setItem("RST_hats", "None_ts.png");
            var t = document.getElementById('hats_img2_ts'),
                s = document.getElementById('hats_chid_ts').getAttribute('src');
            t.style.backgroundImage = 'url(' + s + ')';

        }

    });


    //頭飾
    fs.readdir('./www/assets/tachie/headwear/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_headwear_ts.push(file);

        });

        document.getElementById('headwear_chid_ts').src = './img/None_ts.png';

        var ul = document.getElementById('selert-headwear_ts');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "Theadwear_0");
        li.setAttribute("class", "boty_igm_ts");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addTHeadwear(this)");
        li.setAttribute("imgname", "None_ts.png");
        //設定 img 圖片地址
        img.src = './img/None_ts.png';
        li.appendChild(img);
        ul.appendChild(li);

        for (var i = 0; i < asset_headwear_ts.length; i++) {

            ul = document.getElementById('selert-headwear_ts');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Theadwear_" + i + 1);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTHeadwear(this)");
            li.setAttribute("imgname", asset_headwear_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/headwear/' + asset_headwear_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Theadwear_0').classList.add("fae");
        var img_url = './img/None_ts.png';
        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印

            localStorage.setItem("RST_headwear", "None_ts.png");
            var t = document.getElementById('headwear_img2_ts'),
                s = document.getElementById('headwear_chid_ts').getAttribute('src');
            t.style.backgroundImage = 'url(' + s + ')';

        }

    });


    //上衣
    fs.readdir('./www/assets/tachie/jacket/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_jacket_ts.push(file);

        });

        document.getElementById('jacket_chid_ts').src = './img/None_ts.png';

        var ul = document.getElementById('selert-jacket_ts');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "Tjacket_0");
        li.setAttribute("class", "boty_igm_ts");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addTJacket(this)");
        li.setAttribute("imgname", "None_ts.png");
        //設定 img 圖片地址
        img.src = './img/None_ts.png';
        li.appendChild(img);
        ul.appendChild(li);

        for (var i = 0; i < asset_jacket_ts.length; i++) {

            ul = document.getElementById('selert-jacket_ts');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Tjacket_" + i + 1);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTJacket(this)");
            li.setAttribute("imgname", asset_jacket_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/jacket/' + asset_jacket_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Tjacket_0').classList.add("fae");
        var img_url = './img/None_ts.png';
        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印

            localStorage.setItem("RST_jacket", "None_ts.png");
            var t = document.getElementById('jacket_img2_ts'),
                s = document.getElementById('jacket_chid_ts').getAttribute('src');
            t.style.backgroundImage = 'url(' + s + ')';

        }

    });


    //裙子褲子
    fs.readdir('./www/assets/tachie/skirt_pants/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_skirt_pants_ts.push(file);

        });

        document.getElementById('skirt_pants_chid_ts').src = './img/None_ts.png';

        var ul = document.getElementById('selert-skirt_pants_ts');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "Tskirt_pants_0");
        li.setAttribute("class", "boty_igm_ts");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addTSkirt_pants(this)");
        li.setAttribute("imgname", "None_ts.png");
        //設定 img 圖片地址
        img.src = './img/None_ts.png';
        li.appendChild(img);
        ul.appendChild(li);

        for (var i = 0; i < asset_skirt_pants_ts.length; i++) {

            ul = document.getElementById('selert-skirt_pants_ts');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Tskirt_pants_" + i + 1);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTSkirt_pants(this)");
            li.setAttribute("imgname", asset_skirt_pants_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/skirt_pants/' + asset_skirt_pants_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Tskirt_pants_0').classList.add("fae");
        var img_url = './img/None_ts.png';
        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印

            localStorage.setItem("RST_skirt_pants", "None_ts.png");
            var t = document.getElementById('skirt_pants_img2_ts'),
                s = document.getElementById('skirt_pants_chid_ts').getAttribute('src');
            t.style.backgroundImage = 'url(' + s + ')';

        }

    });



    //襪子
    fs.readdir('./www/assets/tachie/sock/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_sock_ts.push(file);

        });

        document.getElementById('sock_chid_ts').src = './img/None_ts.png';

        var ul = document.getElementById('selert-sock_ts');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "Tsock_0");
        li.setAttribute("class", "boty_igm_ts");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addTSock(this)");
        li.setAttribute("imgname", "None_ts.png");
        //設定 img 圖片地址
        img.src = './img/None_ts.png';
        li.appendChild(img);
        ul.appendChild(li);

        for (var i = 0; i < asset_sock_ts.length; i++) {

            ul = document.getElementById('selert-sock_ts');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Tsock_" + i + 1);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTSock(this)");
            li.setAttribute("imgname", asset_sock_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/sock/' + asset_sock_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Tsock_0').classList.add("fae");
        var img_url = './img/None_ts.png';
        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印

            localStorage.setItem("RST_sock", "None_ts.png");
            var t = document.getElementById('sock_img2_ts'),
                s = document.getElementById('sock_chid_ts').getAttribute('src');
            t.style.backgroundImage = 'url(' + s + ')';

        }

    });


    //鞋子
    fs.readdir('./www/assets/tachie/shoes/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_shoes_ts.push(file);

        });

        document.getElementById('shoes_chid_ts').src = './img/None_ts.png';

        var ul = document.getElementById('selert-shoes_ts');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "Tshoes_0");
        li.setAttribute("class", "boty_igm_ts");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addTShoes(this)");
        li.setAttribute("imgname", "None_ts.png");
        //設定 img 圖片地址
        img.src = './img/None_ts.png';
        li.appendChild(img);
        ul.appendChild(li);

        for (var i = 0; i < asset_shoes_ts.length; i++) {

            ul = document.getElementById('selert-shoes_ts');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Tshoes_" + i + 1);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTShoes(this)");
            li.setAttribute("imgname", asset_shoes_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/shoes/' + asset_shoes_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Tshoes_0').classList.add("fae");
        var img_url = './img/None_ts.png';
        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印

            localStorage.setItem("RST_shoes", "None_ts.png");
            var t = document.getElementById('shoes_img2_ts'),
                s = document.getElementById('shoes_chid_ts').getAttribute('src');
            t.style.backgroundImage = 'url(' + s + ')';

        }

    });

    //尾巴
    fs.readdir('./www/assets/tachie/tail/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_tail_ts.push(file);

        });

        document.getElementById('tail_chid_ts').src = './img/None_ts.png';

        var ul = document.getElementById('selert-tail_ts');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "Ttail_0");
        li.setAttribute("class", "boty_igm_ts");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addTTail(this)");
        li.setAttribute("imgname", "None_ts.png");
        //設定 img 圖片地址
        img.src = './img/None_ts.png';
        li.appendChild(img);
        ul.appendChild(li);

        for (var i = 0; i < asset_tail_ts.length; i++) {

            ul = document.getElementById('selert-tail_ts');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Ttail_" + i + 1);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTTail(this)");
            li.setAttribute("imgname", asset_tail_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/tail/' + asset_tail_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Ttail_0').classList.add("fae");
        var img_url = './img/None_ts.png';
        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印

            localStorage.setItem("RST_tail", "None_ts.png");
            var t = document.getElementById('tail_img2_ts'),
                s = document.getElementById('tail_chid_ts').getAttribute('src');
            t.style.backgroundImage = 'url(' + s + ')';

        }

    });


    //其他
    fs.readdir('./www/assets/tachie/others/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_others_ts.push(file);

        });

        document.getElementById('others_chid_ts').src = './img/None_ts.png';

        var ul = document.getElementById('selert-others_ts');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "Tothers_0");
        li.setAttribute("class", "boty_igm_ts");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addTOthers(this)");
        li.setAttribute("imgname", "None_ts.png");
        //設定 img 圖片地址
        img.src = './img/None_ts.png';
        li.appendChild(img);
        ul.appendChild(li);

        for (var i = 0; i < asset_others_ts.length; i++) {

            ul = document.getElementById('selert-others_ts');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "Tothers_" + i + 1);
            li.setAttribute("class", "boty_igm_ts");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTOthers(this)");
            li.setAttribute("imgname", asset_others_ts[i]);
            //設定 img 圖片地址
            img.src = './assets/tachie/others/' + asset_others_ts[i];
            li.appendChild(img);
            ul.appendChild(li);

        }

        document.getElementById('Tothers_0').classList.add("fae");
        var img_url = './img/None_ts.png';
        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印

            localStorage.setItem("RST_others", "None_ts.png");
            var t = document.getElementById('others_img2_ts'),
                s = document.getElementById('others_chid_ts').getAttribute('src');
            t.style.backgroundImage = 'url(' + s + ')';

        }

    });



}); 