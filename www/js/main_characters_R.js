
/* window.onload = function () {
    var iNow = 0;

    var timer = setInterval(function (argument) {
        if (iNow == 100) {
            clearInterval(timer);
            document.getElementById('progressBox').classList.add('hide');
            document.getElementById('progressBox').classList.remove('show');
            setTimeout(function () {
                document.getElementById('progressBox').style.display = "none";
            }, 400);
        } else {
            iNow += 1;
            progressFn(iNow);
        }

    }, 30);

    function progressFn(cent) {
        var oDiv1 = document.getElementById('progressBox');
        var oDiv2 = document.getElementById('progressBar');
        var oDiv3 = document.getElementById('progressText');

        var allWidth = parseInt(getStyle(oDiv1, 'width'));

        oDiv2.innerHTML = cent + '%';
        oDiv3.innerHTML = cent + '%';
        oDiv2.style.clip = 'rect(0px,' + cent / 100 * allWidth + 'px,40px,0px)';

        function getStyle(obj, attr) {
            if (obj.currentStyle) {
                return obj.currentStyle[attr];
            }
            else {
                return getComputedStyle(obj, false)[attr];
            }
        }
    }
}
 */

function panelDisplay(activePanel, dcl) {
    // Do something...
    let tabLink = document.getElementById(dcl).querySelectorAll("a");
    for (let i = 0; i < tabLink.length; i++) {

        //設定條件tabLink ==activePanel 
        //將tablink代入for循環中並利用 if ...else 進行條件 classList.add增加class="active" ，就是每執行一次function的時候就進行全部tablinks增加class

        if (tabLink[i] == activePanel)
        // 假設目前的 tabLink 等於 activate, 改變他的class .active
        {
            tabLink[i].classList.add("fae");
            // 顯示面板的display:block 
            //  tabContents[i].style.display="block";

        } else {
            // 假設目前的 ablink 不等於 activate, 刪除他的class .active
            tabLink[i].classList.remove("fae");
            //隱藏 面板
            //  tabContents[i].style.display="none";
        }
    }
}




//素體動態
function addBody(e) {	//刪除末列

    panelDisplay(e, "selert-body");
    document.getElementById('body_chid').src = './assets/characters/body/' + e.getAttribute("imgname");

    localStorage.setItem("RS_body", e.getAttribute("imgname"));
    var img_url = './assets/characters/body/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印





        var t = document.getElementById('body_img2'),
            s = document.getElementById('body_chid').getAttribute('src');

        t.style.backgroundImage = 'url(' + s + ')';
        t.style.backgroundSize = img.width / 3 + 'px';
        t.style.width = img.width / 9 + 'px';
        t.style.height = img.height / 12 + 'px';

        var tz = document.getElementById('body_img1')
        tz.style.backgroundImage = 'url(' + s + ')';
        tz.style.backgroundSize = img.width / 3 + 'px';
        tz.style.width = img.width / 9 + 'px';
        tz.style.height = img.height / 18 + 'px';






        var tr_D = document.getElementById('D_body_img3')
        tr_D.style.backgroundImage = 'url(' + s + ')';
        tr_D.style.backgroundSize = img.width / 3 + 'px';
        tr_D.style.width = img.width / 9 + 'px';
        tr_D.style.height = img.height / 12 + 'px';

        var tr_L = document.getElementById('L_body_img3')
        tr_L.style.backgroundImage = 'url(' + s + ')';
        tr_L.style.backgroundSize = img.width / 3 + 'px';
        tr_L.style.width = img.width / 9 + 'px';
        tr_L.style.height = img.height / 12 + 'px';


        var tr_R = document.getElementById('R_body_img3')
        tr_R.style.backgroundImage = 'url(' + s + ')';
        tr_R.style.backgroundSize = img.width / 3 + 'px';
        tr_R.style.width = img.width / 9 + 'px';
        tr_R.style.height = img.height / 12 + 'px';

        var tr_U = document.getElementById('U_body_img3')
        tr_U.style.backgroundImage = 'url(' + s + ')';
        tr_U.style.backgroundSize = img.width / 3 + 'px';
        tr_U.style.width = img.width / 9 + 'px';
        tr_U.style.height = img.height / 12 + 'px';










        const runkeyframes_body = `
    
    @keyframes ball-run{
           0%{
               background-position-x:`+ (-1 * img.width) / 9 + `px;
               background-position-y:`+ (-0 * img.height) / 12 + `px;
           }
           40%{
               background-position-x:`+ (-0 * img.width) / 9 + `px;
               background-position-y:`+ (-0 * img.height) / 12 + `px;
           }
           80%{
               background-position-x:`+ (-1 * img.width) / 9 + `px;
               background-position-y:`+ (-0 * img.height) / 12 + `px;
           }
          
           100%{
               background-position-x:`+ (-2 * img.width) / 9 + `px;
               background-position-y:`+ (-0 * img.height) / 12 + `px;
           }
           
           
       }
    
    @-webkit-keyframes ball-run{
          0%{
               background-position-x:`+ (-1 * img.width) / 9 + `px;
               background-position-y:`+ (-0 * img.height) / 12 + `px;
           }
           40%{
               background-position-x:`+ (-0 * img.width) / 9 + `px;
               background-position-y:`+ (-0 * img.height) / 12 + `px;
           }
           80%{
               background-position-x:`+ (-1 * img.width) / 9 + `px;
               background-position-y:`+ (-0 * img.height) / 12 + `px;
           }
          
           100%{
               background-position-x:`+ (-2 * img.width) / 9 + `px;
               background-position-y:`+ (-0 * img.height) / 12 + `px;
           }
           
           
       }`

    }
}
//眼睛動態
function addEyes(e) {	//刪除末列
    panelDisplay(e, "selert-eyes");
    if (e.getAttribute("imgname") == 'None.png') {
        document.getElementById('eyes_chid').src = './img/None.png';
    } else {
        document.getElementById('eyes_chid').src = './assets/characters/eyes/' + e.getAttribute("imgname");
    }


    localStorage.setItem("RS_eyes", e.getAttribute("imgname"));


    var img_url = e.getAttribute("imgname") == 'None.png' ? './img/None.png' : './assets/characters/eyes/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

    }



    var t = document.getElementById('eyes_img2'),
        s = document.getElementById('eyes_chid').getAttribute('src');

    t.style.backgroundImage = 'url(' + s + ')';
    t.style.backgroundSize = img.width / 3 + 'px';
    t.style.width = img.width / 9 + 'px';
    t.style.height = img.height / 12 + 'px';


    var tz = document.getElementById('eyes_img1')
    tz.style.backgroundImage = 'url(' + s + ')';
    tz.style.backgroundSize = img.width / 3 + 'px';
    tz.style.width = img.width / 9 + 'px';
    tz.style.height = img.height / 18 + 'px';

    var tr_D = document.getElementById('D_eyes_img3')
    tr_D.style.backgroundImage = 'url(' + s + ')';
    tr_D.style.backgroundSize = img.width / 3 + 'px';
    tr_D.style.width = img.width / 9 + 'px';
    tr_D.style.height = img.height / 12 + 'px';

    var tr_L = document.getElementById('L_eyes_img3')
    tr_L.style.backgroundImage = 'url(' + s + ')';
    tr_L.style.backgroundSize = img.width / 3 + 'px';
    tr_L.style.width = img.width / 9 + 'px';
    tr_L.style.height = img.height / 12 + 'px';


    var tr_R = document.getElementById('R_eyes_img3')
    tr_R.style.backgroundImage = 'url(' + s + ')';
    tr_R.style.backgroundSize = img.width / 3 + 'px';
    tr_R.style.width = img.width / 9 + 'px';
    tr_R.style.height = img.height / 12 + 'px';

    var tr_U = document.getElementById('U_eyes_img3')
    tr_U.style.backgroundImage = 'url(' + s + ')';
    tr_U.style.backgroundSize = img.width / 3 + 'px';
    tr_U.style.width = img.width / 9 + 'px';
    tr_U.style.height = img.height / 12 + 'px';


    const runkeyframes_eyes = `

@keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}

@-webkit-keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}`


};

//眉毛動態
function addEyebrow(e) {	//刪除末列
    panelDisplay(e, "selert-eyebrow");

    if (e.getAttribute("imgname") == 'None.png') {
        document.getElementById('eyebrow_chid').src = './img/None.png';
    } else {
        document.getElementById('eyebrow_chid').src = './assets/characters/eyebrow/' + e.getAttribute("imgname");
    }


    localStorage.setItem("RS_eyebrow", e.getAttribute("imgname"));


    var img_url = e.getAttribute("imgname") == 'None.png' ? './img/None.png' : './assets/characters/eyebrow/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

    }



    var t = document.getElementById('eyebrow_img2'),
        s = document.getElementById('eyebrow_chid').getAttribute('src');

    t.style.backgroundImage = 'url(' + s + ')';
    t.style.backgroundSize = img.width / 3 + 'px';
    t.style.width = img.width / 9 + 'px';
    t.style.height = img.height / 12 + 'px';


    var tz = document.getElementById('eyebrow_img1')
    tz.style.backgroundImage = 'url(' + s + ')';
    tz.style.backgroundSize = img.width / 3 + 'px';
    tz.style.width = img.width / 9 + 'px';
    tz.style.height = img.height / 18 + 'px';

    var tr_D = document.getElementById('D_eyebrow_img3')
    tr_D.style.backgroundImage = 'url(' + s + ')';
    tr_D.style.backgroundSize = img.width / 3 + 'px';
    tr_D.style.width = img.width / 9 + 'px';
    tr_D.style.height = img.height / 12 + 'px';

    var tr_L = document.getElementById('L_eyebrow_img3')
    tr_L.style.backgroundImage = 'url(' + s + ')';
    tr_L.style.backgroundSize = img.width / 3 + 'px';
    tr_L.style.width = img.width / 9 + 'px';
    tr_L.style.height = img.height / 12 + 'px';


    var tr_R = document.getElementById('R_eyebrow_img3')
    tr_R.style.backgroundImage = 'url(' + s + ')';
    tr_R.style.backgroundSize = img.width / 3 + 'px';
    tr_R.style.width = img.width / 9 + 'px';
    tr_R.style.height = img.height / 12 + 'px';

    var tr_U = document.getElementById('U_eyebrow_img3')
    tr_U.style.backgroundImage = 'url(' + s + ')';
    tr_U.style.backgroundSize = img.width / 3 + 'px';
    tr_U.style.width = img.width / 9 + 'px';
    tr_U.style.height = img.height / 12 + 'px';


    const runkeyframes_eyebrow = `

@keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}

@-webkit-keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}`


};


//髮型動態
function addHairs(e) {	//刪除末列

    panelDisplay(e, "selert-hairs");

    if (e.getAttribute("imgname") == 'None.png') {
        document.getElementById('hairs_chid').src = './img/None.png';
    } else {
        document.getElementById('hairs_chid').src = './assets/characters/hairs/' + e.getAttribute("imgname");
    }


    localStorage.setItem("RS_hairs", e.getAttribute("imgname"));



    var img_url = e.getAttribute("imgname") == 'None.png' ? './img/None.png' : './assets/characters/hairs/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

    }




    var t = document.getElementById('hairs_img2'),
        s = document.getElementById('hairs_chid').getAttribute('src');

    t.style.backgroundImage = 'url(' + s + ')';
    t.style.backgroundSize = img.width / 3 + 'px';
    t.style.width = img.width / 9 + 'px';
    t.style.height = img.height / 12 + 'px';

    var tz = document.getElementById('hairs_img1')
    tz.style.backgroundImage = 'url(' + s + ')';
    tz.style.backgroundSize = img.width / 3 + 'px';
    tz.style.width = img.width / 9 + 'px';
    tz.style.height = img.height / 18 + 'px';


    var tr_D = document.getElementById('D_hairs_img3')
    tr_D.style.backgroundImage = 'url(' + s + ')';
    tr_D.style.backgroundSize = img.width / 3 + 'px';
    tr_D.style.width = img.width / 9 + 'px';
    tr_D.style.height = img.height / 12 + 'px';

    var tr_L = document.getElementById('L_hairs_img3')
    tr_L.style.backgroundImage = 'url(' + s + ')';
    tr_L.style.backgroundSize = img.width / 3 + 'px';
    tr_L.style.width = img.width / 9 + 'px';
    tr_L.style.height = img.height / 12 + 'px';

    var tr_R = document.getElementById('R_hairs_img3')
    tr_R.style.backgroundImage = 'url(' + s + ')';
    tr_R.style.backgroundSize = img.width / 3 + 'px';
    tr_R.style.width = img.width / 9 + 'px';
    tr_R.style.height = img.height / 12 + 'px';

    var tr_U = document.getElementById('U_hairs_img3')
    tr_U.style.backgroundImage = 'url(' + s + ')';
    tr_U.style.backgroundSize = img.width / 3 + 'px';
    tr_U.style.width = img.width / 9 + 'px';
    tr_U.style.height = img.height / 12 + 'px';





    const runkeyframes_hairs = `

@keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}

@-webkit-keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}`


};


//眼鏡動態
function addGlasses(e) {	//刪除末列
    panelDisplay(e, "selert-glasses");
    if (e.getAttribute("imgname") == 'None.png') {
        document.getElementById('glasses_chid').src = './img/None.png';
    } else {
        document.getElementById('glasses_chid').src = './assets/characters/glasses/' + e.getAttribute("imgname");
    }





    localStorage.setItem("RS_glasses", e.getAttribute("imgname"));

    var img_url = e.getAttribute("imgname") == 'None.png' ? './img/None.png' : './assets/characters/glasses/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

    }




    var t = document.getElementById('glasses_img2'),
        s = document.getElementById('glasses_chid').getAttribute('src');

    t.style.backgroundImage = 'url(' + s + ')';
    t.style.backgroundSize = img.width / 3 + 'px';
    t.style.width = img.width / 9 + 'px';
    t.style.height = img.height / 12 + 'px';

    var tz = document.getElementById('glasses_img1')
    tz.style.backgroundImage = 'url(' + s + ')';
    tz.style.backgroundSize = img.width / 3 + 'px';
    tz.style.width = img.width / 9 + 'px';
    tz.style.height = img.height / 18 + 'px';


    var tr_D = document.getElementById('D_glasses_img3')
    tr_D.style.backgroundImage = 'url(' + s + ')';
    tr_D.style.backgroundSize = img.width / 3 + 'px';
    tr_D.style.width = img.width / 9 + 'px';
    tr_D.style.height = img.height / 12 + 'px';

    var tr_L = document.getElementById('L_glasses_img3')
    tr_L.style.backgroundImage = 'url(' + s + ')';
    tr_L.style.backgroundSize = img.width / 3 + 'px';
    tr_L.style.width = img.width / 9 + 'px';
    tr_L.style.height = img.height / 12 + 'px';

    var tr_R = document.getElementById('R_glasses_img3')
    tr_R.style.backgroundImage = 'url(' + s + ')';
    tr_R.style.backgroundSize = img.width / 3 + 'px';
    tr_R.style.width = img.width / 9 + 'px';
    tr_R.style.height = img.height / 12 + 'px';

    var tr_U = document.getElementById('U_glasses_img3')
    tr_U.style.backgroundImage = 'url(' + s + ')';
    tr_U.style.backgroundSize = img.width / 3 + 'px';
    tr_U.style.width = img.width / 9 + 'px';
    tr_U.style.height = img.height / 12 + 'px';





    const runkeyframes_glasses = `

@keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}

@-webkit-keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}`


};



//嘴巴動態
function addMouth(e) {	//刪除末列
    panelDisplay(e, "selert-mouth");
    if (e.getAttribute("imgname") == 'None.png') {
        document.getElementById('mouth_chid').src = './img/None.png';
    } else {
        document.getElementById('mouth_chid').src = './assets/characters/mouth/' + e.getAttribute("imgname");
    }





    localStorage.setItem("RS_mouth", e.getAttribute("imgname"));

    var img_url = e.getAttribute("imgname") == 'None.png' ? './img/None.png' : './assets/characters/mouth/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

    }




    var t = document.getElementById('mouth_img2'),
        s = document.getElementById('mouth_chid').getAttribute('src');

    t.style.backgroundImage = 'url(' + s + ')';
    t.style.backgroundSize = img.width / 3 + 'px';
    t.style.width = img.width / 9 + 'px';
    t.style.height = img.height / 12 + 'px';

    var tz = document.getElementById('mouth_img1')
    tz.style.backgroundImage = 'url(' + s + ')';
    tz.style.backgroundSize = img.width / 3 + 'px';
    tz.style.width = img.width / 9 + 'px';
    tz.style.height = img.height / 18 + 'px';


    var tr_D = document.getElementById('D_mouth_img3')
    tr_D.style.backgroundImage = 'url(' + s + ')';
    tr_D.style.backgroundSize = img.width / 3 + 'px';
    tr_D.style.width = img.width / 9 + 'px';
    tr_D.style.height = img.height / 12 + 'px';

    var tr_L = document.getElementById('L_mouth_img3')
    tr_L.style.backgroundImage = 'url(' + s + ')';
    tr_L.style.backgroundSize = img.width / 3 + 'px';
    tr_L.style.width = img.width / 9 + 'px';
    tr_L.style.height = img.height / 12 + 'px';

    var tr_R = document.getElementById('R_mouth_img3')
    tr_R.style.backgroundImage = 'url(' + s + ')';
    tr_R.style.backgroundSize = img.width / 3 + 'px';
    tr_R.style.width = img.width / 9 + 'px';
    tr_R.style.height = img.height / 12 + 'px';

    var tr_U = document.getElementById('U_mouth_img3')
    tr_U.style.backgroundImage = 'url(' + s + ')';
    tr_U.style.backgroundSize = img.width / 3 + 'px';
    tr_U.style.width = img.width / 9 + 'px';
    tr_U.style.height = img.height / 12 + 'px';





    const runkeyframes_mouth = `

@keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}

@-webkit-keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}`


};


//衣服下半動態
function addLower_Clothes(e) {	//刪除末列
    panelDisplay(e, "selert-lower_clothes");
    if (e.getAttribute("imgname") == 'None.png') {
        document.getElementById('lower_clothes_chid').src = './img/None.png';
    } else {
        document.getElementById('lower_clothes_chid').src = './assets/characters/lower_clothes/' + e.getAttribute("imgname");
    }





    localStorage.setItem("RS_lower_clothes", e.getAttribute("imgname"));

    var img_url = e.getAttribute("imgname") == 'None.png' ? './img/None.png' : './assets/characters/lower_clothes/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

    }




    var t = document.getElementById('lower_clothes_img2'),
        s = document.getElementById('lower_clothes_chid').getAttribute('src');

    t.style.backgroundImage = 'url(' + s + ')';
    t.style.backgroundSize = img.width / 3 + 'px';
    t.style.width = img.width / 9 + 'px';
    t.style.height = img.height / 12 + 'px';

    var tz = document.getElementById('lower_clothes_img1')
    tz.style.backgroundImage = 'url(' + s + ')';
    tz.style.backgroundSize = img.width / 3 + 'px';
    tz.style.width = img.width / 9 + 'px';
    tz.style.height = img.height / 18 + 'px';


    var tr_D = document.getElementById('D_lower_clothes_img3')
    tr_D.style.backgroundImage = 'url(' + s + ')';
    tr_D.style.backgroundSize = img.width / 3 + 'px';
    tr_D.style.width = img.width / 9 + 'px';
    tr_D.style.height = img.height / 12 + 'px';

    var tr_L = document.getElementById('L_lower_clothes_img3')
    tr_L.style.backgroundImage = 'url(' + s + ')';
    tr_L.style.backgroundSize = img.width / 3 + 'px';
    tr_L.style.width = img.width / 9 + 'px';
    tr_L.style.height = img.height / 12 + 'px';

    var tr_R = document.getElementById('R_lower_clothes_img3')
    tr_R.style.backgroundImage = 'url(' + s + ')';
    tr_R.style.backgroundSize = img.width / 3 + 'px';
    tr_R.style.width = img.width / 9 + 'px';
    tr_R.style.height = img.height / 12 + 'px';

    var tr_U = document.getElementById('U_lower_clothes_img3')
    tr_U.style.backgroundImage = 'url(' + s + ')';
    tr_U.style.backgroundSize = img.width / 3 + 'px';
    tr_U.style.width = img.width / 9 + 'px';
    tr_U.style.height = img.height / 12 + 'px';





    const runkeyframes_lower_clothes = `

@keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}

@-webkit-keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}`


};





//衣服上半動態
function addTop_Clothes(e) {	//刪除末列

    panelDisplay(e, "selert-top_clothes");

    if (e.getAttribute("imgname") == 'None.png') {
        document.getElementById('top_clothes_chid').src = './img/None.png';
    } else {
        document.getElementById('top_clothes_chid').src = './assets/characters/top_clothes/' + e.getAttribute("imgname");
    }



    localStorage.setItem("RS_top_clothes", e.getAttribute("imgname"));

    var img_url = e.getAttribute("imgname") == 'None.png' ? './img/None.png' : './assets/characters/top_clothes/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

    }




    var t = document.getElementById('top_clothes_img2'),
        s = document.getElementById('top_clothes_chid').getAttribute('src');

    t.style.backgroundImage = 'url(' + s + ')';
    t.style.backgroundSize = img.width / 3 + 'px';
    t.style.width = img.width / 9 + 'px';
    t.style.height = img.height / 12 + 'px';

    var tz = document.getElementById('top_clothes_img1')
    tz.style.backgroundImage = 'url(' + s + ')';
    tz.style.backgroundSize = img.width / 3 + 'px';
    tz.style.width = img.width / 9 + 'px';
    tz.style.height = img.height / 18 + 'px';


    var tr_D = document.getElementById('D_top_clothes_img3')
    tr_D.style.backgroundImage = 'url(' + s + ')';
    tr_D.style.backgroundSize = img.width / 3 + 'px';
    tr_D.style.width = img.width / 9 + 'px';
    tr_D.style.height = img.height / 12 + 'px';

    var tr_L = document.getElementById('L_top_clothes_img3')
    tr_L.style.backgroundImage = 'url(' + s + ')';
    tr_L.style.backgroundSize = img.width / 3 + 'px';
    tr_L.style.width = img.width / 9 + 'px';
    tr_L.style.height = img.height / 12 + 'px';

    var tr_R = document.getElementById('R_top_clothes_img3')
    tr_R.style.backgroundImage = 'url(' + s + ')';
    tr_R.style.backgroundSize = img.width / 3 + 'px';
    tr_R.style.width = img.width / 9 + 'px';
    tr_R.style.height = img.height / 12 + 'px';

    var tr_U = document.getElementById('U_top_clothes_img3')
    tr_U.style.backgroundImage = 'url(' + s + ')';
    tr_U.style.backgroundSize = img.width / 3 + 'px';
    tr_U.style.width = img.width / 9 + 'px';
    tr_U.style.height = img.height / 12 + 'px';





    const runkeyframes_top_clothes = `

@keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}

@-webkit-keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}`


};


//帽子動態
function addHats(e) {	//刪除末列

    panelDisplay(e, "selert-hats");
    if (e.getAttribute("imgname") == 'None.png') {
        document.getElementById('hats_chid').src = './img/None.png';
    } else {


        document.getElementById('hats_chid').src = './assets/characters/hats/' + e.getAttribute("imgname");
    }



    localStorage.setItem("RS_hats", e.getAttribute("imgname"));

    var img_url = e.getAttribute("imgname") == 'None.png' ? './img/None.png' : './assets/characters/hats/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

    }




    var t = document.getElementById('hats_img2'),
        s = document.getElementById('hats_chid').getAttribute('src');

    t.style.backgroundImage = 'url(' + s + ')';
    t.style.backgroundSize = img.width / 3 + 'px';
    t.style.width = img.width / 9 + 'px';
    t.style.height = img.height / 12 + 'px';

    var tz = document.getElementById('hats_img1')
    tz.style.backgroundImage = 'url(' + s + ')';
    tz.style.backgroundSize = img.width / 3 + 'px';
    tz.style.width = img.width / 9 + 'px';
    tz.style.height = img.height / 18 + 'px';


    var tr_D = document.getElementById('D_hats_img3')
    tr_D.style.backgroundImage = 'url(' + s + ')';
    tr_D.style.backgroundSize = img.width / 3 + 'px';
    tr_D.style.width = img.width / 9 + 'px';
    tr_D.style.height = img.height / 12 + 'px';

    var tr_L = document.getElementById('L_hats_img3')
    tr_L.style.backgroundImage = 'url(' + s + ')';
    tr_L.style.backgroundSize = img.width / 3 + 'px';
    tr_L.style.width = img.width / 9 + 'px';
    tr_L.style.height = img.height / 12 + 'px';

    var tr_R = document.getElementById('R_hats_img3')
    tr_R.style.backgroundImage = 'url(' + s + ')';
    tr_R.style.backgroundSize = img.width / 3 + 'px';
    tr_R.style.width = img.width / 9 + 'px';
    tr_R.style.height = img.height / 12 + 'px';

    var tr_U = document.getElementById('U_hats_img3')
    tr_U.style.backgroundImage = 'url(' + s + ')';
    tr_U.style.backgroundSize = img.width / 3 + 'px';
    tr_U.style.width = img.width / 9 + 'px';
    tr_U.style.height = img.height / 12 + 'px';





    const runkeyframes_hats = `

@keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}

@-webkit-keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}`


};





//飾品1動態
function addAccessories_01(e) {	//刪除末列

    panelDisplay(e, "selert-accessories_01");


    if (e.getAttribute("imgname") == 'None.png') {
        document.getElementById('accessories_01_chid').src = './img/None.png';
    } else {

        document.getElementById('accessories_01_chid').src = './assets/characters/accessories_01/' + e.getAttribute("imgname");
    }



    localStorage.setItem("RS_accessories_01", e.getAttribute("imgname"));



    var img_url = e.getAttribute("imgname") == 'None.png' ? './img/None.png' : './assets/characters/accessories_01/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

    }




    var t = document.getElementById('accessories_01_img2'),
        s = document.getElementById('accessories_01_chid').getAttribute('src');

    t.style.backgroundImage = 'url(' + s + ')';
    t.style.backgroundSize = img.width / 3 + 'px';
    t.style.width = img.width / 9 + 'px';
    t.style.height = img.height / 12 + 'px';

    var tz = document.getElementById('accessories_01_img1')
    tz.style.backgroundImage = 'url(' + s + ')';
    tz.style.backgroundSize = img.width / 3 + 'px';
    tz.style.width = img.width / 9 + 'px';
    tz.style.height = img.height / 18 + 'px';

    var tr_D = document.getElementById('D_accessories_01_img3')
    tr_D.style.backgroundImage = 'url(' + s + ')';
    tr_D.style.backgroundSize = img.width / 3 + 'px';
    tr_D.style.width = img.width / 9 + 'px';
    tr_D.style.height = img.height / 12 + 'px';

    var tr_L = document.getElementById('L_accessories_01_img3')
    tr_L.style.backgroundImage = 'url(' + s + ')';
    tr_L.style.backgroundSize = img.width / 3 + 'px';
    tr_L.style.width = img.width / 9 + 'px';
    tr_L.style.height = img.height / 12 + 'px';


    var tr_R = document.getElementById('R_accessories_01_img3')
    tr_R.style.backgroundImage = 'url(' + s + ')';
    tr_R.style.backgroundSize = img.width / 3 + 'px';
    tr_R.style.width = img.width / 9 + 'px';
    tr_R.style.height = img.height / 12 + 'px';

    var tr_U = document.getElementById('U_accessories_01_img3')
    tr_U.style.backgroundImage = 'url(' + s + ')';
    tr_U.style.backgroundSize = img.width / 3 + 'px';
    tr_U.style.width = img.width / 9 + 'px';
    tr_U.style.height = img.height / 12 + 'px';

    const runkeyframes_accessories_01 = `

@keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}

@-webkit-keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}`


};



//飾品2動態
function addAccessories_02(e) {	//刪除末列

    panelDisplay(e, "selert-accessories_02");


    if (e.getAttribute("imgname") == 'None.png') {
        document.getElementById('accessories_02_chid').src = './img/None.png';
    } else {


        document.getElementById('accessories_02_chid').src = './assets/characters/accessories_02/' + e.getAttribute("imgname");
    }

    localStorage.setItem("RS_accessories_02", e.getAttribute("imgname"));



    var img_url = e.getAttribute("imgname") == 'None.png' ? './img/None.png' : './assets/characters/accessories_02/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

    }




    var t = document.getElementById('accessories_02_img2'),
        s = document.getElementById('accessories_02_chid').getAttribute('src');

    t.style.backgroundImage = 'url(' + s + ')';
    t.style.backgroundSize = img.width / 3 + 'px';
    t.style.width = img.width / 9 + 'px';
    t.style.height = img.height / 12 + 'px';

    var tz = document.getElementById('accessories_02_img1')
    tz.style.backgroundImage = 'url(' + s + ')';
    tz.style.backgroundSize = img.width / 3 + 'px';
    tz.style.width = img.width / 9 + 'px';
    tz.style.height = img.height / 18 + 'px';

    var tr_D = document.getElementById('D_accessories_02_img3')
    tr_D.style.backgroundImage = 'url(' + s + ')';
    tr_D.style.backgroundSize = img.width / 3 + 'px';
    tr_D.style.width = img.width / 9 + 'px';
    tr_D.style.height = img.height / 12 + 'px';

    var tr_L = document.getElementById('L_accessories_02_img3')
    tr_L.style.backgroundImage = 'url(' + s + ')';
    tr_L.style.backgroundSize = img.width / 3 + 'px';
    tr_L.style.width = img.width / 9 + 'px';
    tr_L.style.height = img.height / 12 + 'px';


    var tr_R = document.getElementById('R_accessories_02_img3')
    tr_R.style.backgroundImage = 'url(' + s + ')';
    tr_R.style.backgroundSize = img.width / 3 + 'px';
    tr_R.style.width = img.width / 9 + 'px';
    tr_R.style.height = img.height / 12 + 'px';

    var tr_U = document.getElementById('U_accessories_02_img3')
    tr_U.style.backgroundImage = 'url(' + s + ')';
    tr_U.style.backgroundSize = img.width / 3 + 'px';
    tr_U.style.width = img.width / 9 + 'px';
    tr_U.style.height = img.height / 12 + 'px';

    const runkeyframes_accessories_02 = `

@keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}

@-webkit-keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}`


};




//其他動態
function addOthers(e) {	//刪除末列

    panelDisplay(e, "selert-others");

    if (e.getAttribute("imgname") == 'None.png') {
        document.getElementById('others_chid').src = './img/None.png';
    } else {

        document.getElementById('others_chid').src = './assets/characters/others/' + e.getAttribute("imgname");
    }


    localStorage.setItem("RS_others", e.getAttribute("imgname"));


    var img_url = e.getAttribute("imgname") == 'None.png' ? './img/None.png' : './assets/characters/others/' + e.getAttribute("imgname");


    var img = new Image()

    // 改变图片的src
    img.src = img_url

    // 加载完成执行
    img.onload = function () {
        // 打印

    }




    var t = document.getElementById('others_img2'),
        s = document.getElementById('others_chid').getAttribute('src');

    t.style.backgroundImage = 'url(' + s + ')';
    t.style.backgroundSize = img.width / 3 + 'px';
    t.style.width = img.width / 9 + 'px';
    t.style.height = img.height / 12 + 'px';

    var tz = document.getElementById('others_img1')
    tz.style.backgroundImage = 'url(' + s + ')';
    tz.style.backgroundSize = img.width / 3 + 'px';
    tz.style.width = img.width / 9 + 'px';
    tz.style.height = img.height / 18 + 'px';


    var tr_D = document.getElementById('D_others_img3')
    tr_D.style.backgroundImage = 'url(' + s + ')';
    tr_D.style.backgroundSize = img.width / 3 + 'px';
    tr_D.style.width = img.width / 9 + 'px';
    tr_D.style.height = img.height / 12 + 'px';

    var tr_L = document.getElementById('L_others_img3')
    tr_L.style.backgroundImage = 'url(' + s + ')';
    tr_L.style.backgroundSize = img.width / 3 + 'px';
    tr_L.style.width = img.width / 9 + 'px';
    tr_L.style.height = img.height / 12 + 'px';

    var tr_R = document.getElementById('R_others_img3')
    tr_R.style.backgroundImage = 'url(' + s + ')';
    tr_R.style.backgroundSize = img.width / 3 + 'px';
    tr_R.style.width = img.width / 9 + 'px';
    tr_R.style.height = img.height / 12 + 'px';

    var tr_U = document.getElementById('U_others_img3')
    tr_U.style.backgroundImage = 'url(' + s + ')';
    tr_U.style.backgroundSize = img.width / 3 + 'px';
    tr_U.style.width = img.width / 9 + 'px';
    tr_U.style.height = img.height / 12 + 'px';




    const runkeyframes_others = `

@keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}

@-webkit-keyframes ball-run{
0%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * img.width) / 9 + `px;
   background-position-y:`+ (-0 * img.height) / 12 + `px;
}


}`


};


document.addEventListener("DOMContentLoaded", function () {

    //讀取目錄
    var fs = require('fs');
    var asset_body = [];
    var asset_eyes = [];
    var asset_eyebrow = [];
    var asset_hairs = [];
    var asset_glasses = [];
    var asset_mouth = [];
    var asset_lower_clothes = [];
    var asset_top_clothes = [];
    var asset_hats = [];
    var asset_accessories_01 = [];
    var asset_accessories_02 = [];
    var asset_others = [];
    //如果路徑指定不規範，則會按當前檔案所在目錄處理
    //如果為目錄不存在丟擲異常 { [Error: ENOENT, scandir 'F:\test1'] errno: -4058, code: 'ENOENT', path: 'F:\\test1' }

    localStorage.setItem("WS_direct", "Down");
    localStorage.setItem("WS_anime", "Play");

    localStorage.setItem("WS_width", null);
    localStorage.setItem("WS_height", null);

    localStorage.setItem("RS_body", null);
    localStorage.setItem("RS_eyes", null);
    localStorage.setItem("RS_eyebrow", null);
    localStorage.setItem("RS_hairs", null);
    localStorage.setItem("RS_glasses", null);
    localStorage.setItem("RS_mouth", null);
    localStorage.setItem("RS_lower_clothes", null);
    localStorage.setItem("RS_top_clothes", null);
    localStorage.setItem("RS_hats", null);
    localStorage.setItem("RS_accessories_01", null);
    localStorage.setItem("RS_accessories_02", null);
    localStorage.setItem("RS_others", null);



    //素體


    fs.readdir('./www/assets/characters/body/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_body.push(file);

        });

        document.getElementById('body_chid').src = './assets/characters/body/' + asset_body[0];




        for (var i = 0; i < asset_body.length; i++) {
            // document.getElementById('pills-body').appendChild

            var ul = document.getElementById('selert-body');
            //新增 li
            var li = document.createElement("a");
            //新增 img
            var img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "body_" + i);
            li.setAttribute("class", "boty_igm");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addBody(this)");
            li.setAttribute("imgname", asset_body[i]);
            //設定 img 圖片地址
            img.src = './assets/characters/body/' + asset_body[i];
            li.appendChild(img);
            ul.appendChild(li);






        }

        document.getElementById('body_0').classList.add("fae");


        var img_url = './assets/characters/body/' + asset_body[0];


        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印


            localStorage.setItem("WS_width", img.width);
            localStorage.setItem("WS_height", img.height);


            localStorage.setItem("RS_body", asset_body[0]);



            var t = document.getElementById('body_img2'),
                s = document.getElementById('body_chid').getAttribute('src');

            t.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run 0.5s step-start 0s infinite alternate;');

            var tz = document.getElementById('body_img1')

            tz.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 18 + 'px;width:' + img.width / 9 + 'px;background-position-x:-42px;background-position-y:' + (-0 * img.height) / 12 + 'px;');



            var tr_D = document.getElementById('D_body_img3')

            tr_D.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-D 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-D 0.5s step-start 0s infinite alternate;');

            var tr_L = document.getElementById('L_body_img3')

            tr_L.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-L 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-L 0.5s step-start 0s infinite alternate;');

            var tr_R = document.getElementById('R_body_img3')

            tr_R.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-R 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-R 0.5s step-start 0s infinite alternate;');

            var tr_U = document.getElementById('U_body_img3')

            tr_U.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-U 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-U 0.5s step-start 0s infinite alternate;');



        }


        setTimeout(function () {



            const runkeyframes_body = `
            @keyframes ball-run{
                0%{
                   background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                   background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
                }
                40%{
                   background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                   background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
                }
                80%{
                   background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                   background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
                }
                
                100%{
                   background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                   background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
                }
                
                
                }
                
                @-webkit-keyframes ball-run{
                    0%{
                        background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                        background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
                     }
                     40%{
                        background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                        background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
                     }
                     80%{
                        background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                        background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
                     }
                     
                     100%{
                        background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                        background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
                     }
                
                
                }

@keyframes ball-run-D{
0%{
   background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
   background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
}
40%{
   background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
   background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
}
80%{
   background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
   background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
}

100%{
   background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
   background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
}


}

@-webkit-keyframes ball-run-D{
    0%{
        background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
        background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
     }
     40%{
        background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
        background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
     }
     80%{
        background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
        background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
     }
     
     100%{
        background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
        background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
     }


}


@keyframes ball-run-L{
    0%{
       background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
       background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
    }
    40%{
       background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
       background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
    }
    80%{
       background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
       background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
    }
    
    100%{
       background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
       background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
    }
    
    
    }
    
    @-webkit-keyframes ball-run-L{
        0%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         40%{
            background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         80%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         
         100%{
            background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
    
    
    }


    @keyframes ball-run-R{
        0%{
           background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
           background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
        }
        40%{
           background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
           background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
        }
        80%{
           background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
           background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
        }
        
        100%{
           background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
           background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
        }
        
        
        }
        
        @-webkit-keyframes ball-run-R{
            0%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             40%{
                background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             80%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             100%{
                background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
        
        
        }

        @keyframes ball-run-U{
            0%{
               background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
               background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
            }
            40%{
               background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
               background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
            }
            80%{
               background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
               background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
            }
            
            100%{
               background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
               background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
            }
            
            
            }
            
            @-webkit-keyframes ball-run-U{
                0%{
                    background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                    background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
                 }
                 40%{
                    background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                    background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
                 }
                 80%{
                    background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                    background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
                 }
                 
                 100%{
                    background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                    background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
                 }
            
            
            }

`


            const style = document.createElement('style');
            // 设置style属性
            style.type = 'text/css';
            // 将 keyframes样式写入style内
            style.innerHTML = runkeyframes_body;
            // 将style样式存放到head标签
            document.head.appendChild(style);
        }, 800);
        // 创建style标签




    });





    //正面
    var btnL = document.getElementById("btn_animeleft");
    btnL.addEventListener('click', function () {


        if (localStorage.getItem("WS_anime") == "Play") {


            if (localStorage.getItem("WS_direct") == "Down") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                var ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                var runkeyframesg_body = `
     @keyframes ball-run{
         0%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         40%{
            background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         80%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         
         100%{
            background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         
         
         }
     
`
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);


                localStorage.setItem("WS_direct", "Left");
                return false;
            }

            if (localStorage.getItem("WS_direct") == "Left") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
     @keyframes ball-run{
         0%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         40%{
            background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         80%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         
         100%{
            background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         
         
         }
     
`
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);


                localStorage.setItem("WS_direct", "Up");
                return false;
            }
            if (localStorage.getItem("WS_direct") == "Up") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
     @keyframes ball-run{
         0%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         40%{
            background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         80%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         
         100%{
            background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         
         
         }
     
`
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);


                localStorage.setItem("WS_direct", "Right");
                return false;
            }

            if (localStorage.getItem("WS_direct") == "Right") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
     @keyframes ball-run{
         0%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         40%{
            background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         80%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         
         100%{
            background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         
         
         }
     
`
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);


                localStorage.setItem("WS_direct", "Down");
                return false;
            }


        } else {
            if (localStorage.getItem("WS_direct") == "Down") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                var ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                var runkeyframesg_body = `
     @keyframes ball-run{
         0%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         40%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         80%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         
         100%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         
         
         }
     
`
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);


                localStorage.setItem("WS_direct", "Left");
                return false;
            }

            if (localStorage.getItem("WS_direct") == "Left") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
     @keyframes ball-run{
         0%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         40%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         80%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         
         100%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         
         
         }
     
`
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);


                localStorage.setItem("WS_direct", "Up");
                return false;
            }
            if (localStorage.getItem("WS_direct") == "Up") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
     @keyframes ball-run{
         0%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         40%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         80%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         
         100%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         
         
         }
     
`
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);


                localStorage.setItem("WS_direct", "Right");
                return false;
            }

            if (localStorage.getItem("WS_direct") == "Right") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
     @keyframes ball-run{
         0%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         40%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         80%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         
         100%{
            background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
            background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
         }
         
         
         }
     
`
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);


                localStorage.setItem("WS_direct", "Down");
                return false;
            }

        }


    }, false);



    //------------------------------------------------------

    var btnR = document.getElementById("btn_animeright");
    btnR.addEventListener('click', function () {
        if (localStorage.getItem("WS_anime") == "Play") {

            if (localStorage.getItem("WS_direct") == "Down") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                var ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                var runkeyframesg_body = `
         @keyframes ball-run{
             0%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             40%{
                background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             80%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             100%{
                background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             
             }
         
    `
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);


                localStorage.setItem("WS_direct", "Right");
                return false;
            }

            if (localStorage.getItem("WS_direct") == "Right") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
         @keyframes ball-run{
             0%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             40%{
                background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             80%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             100%{
                background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             
             }
         
    `
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);


                localStorage.setItem("WS_direct", "Up");
                return false;
            }
            if (localStorage.getItem("WS_direct") == "Up") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
         @keyframes ball-run{
             0%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             40%{
                background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             80%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             100%{
                background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             
             }
         
    `
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);


                localStorage.setItem("WS_direct", "Left");
                return false;
            }

            if (localStorage.getItem("WS_direct") == "Left") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
         @keyframes ball-run{
             0%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             40%{
                background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             80%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             100%{
                background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             
             }
         
    `
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);


                localStorage.setItem("WS_direct", "Down");
                return false;
            }

        } else {

            if (localStorage.getItem("WS_direct") == "Down") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                var ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                var runkeyframesg_body = `
         @keyframes ball-run{
             0%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             40%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             80%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             100%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             
             }
         
    `
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);


                localStorage.setItem("WS_direct", "Right");
                return false;
            }

            if (localStorage.getItem("WS_direct") == "Right") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
         @keyframes ball-run{
             0%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             40%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             80%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             100%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             
             }
         
    `
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);


                localStorage.setItem("WS_direct", "Up");
                return false;
            }
            if (localStorage.getItem("WS_direct") == "Up") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
         @keyframes ball-run{
             0%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             40%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             80%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             100%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             
             }
         
    `
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);


                localStorage.setItem("WS_direct", "Left");
                return false;
            }

            if (localStorage.getItem("WS_direct") == "Left") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
         @keyframes ball-run{
             0%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             40%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             80%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             100%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             
             }
         
    `
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);


                localStorage.setItem("WS_direct", "Down");
                return false;
            }

        }


    }, false);

    //------------------------------------------------------

    var btnP = document.getElementById("btn_animeplay");
    btnP.addEventListener('click', function () {



        if (localStorage.getItem("WS_anime") == "Play") {


            if (localStorage.getItem("WS_direct") == "Down") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
         @keyframes ball-run{
             0%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             40%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             80%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             100%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             
             }
         
    `
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);

            }
            if (localStorage.getItem("WS_direct") == "Left") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
         @keyframes ball-run{
             0%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             40%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             80%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             100%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             
             }
         
    `
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);

            }

            if (localStorage.getItem("WS_direct") == "Right") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
         @keyframes ball-run{
             0%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             40%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             80%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             100%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             
             }
         
    `
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);

            }


            if (localStorage.getItem("WS_direct") == "Up") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
         @keyframes ball-run{
             0%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             40%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             80%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             100%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             
             }
         
    `
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);

            }
            var uptInput = document.getElementById('btn_animeplay');

            uptInput.src = "./img/neko_animplay.png"
            localStorage.setItem("WS_anime", "Pause");
            return false;
        } else {

            if (localStorage.getItem("WS_direct") == "Down") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
         @keyframes ball-run{
             0%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             40%{
                background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             80%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             100%{
                background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-0 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             
             }
         
    `
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);

            }
            if (localStorage.getItem("WS_direct") == "Left") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
         @keyframes ball-run{
             0%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             40%{
                background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             80%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             100%{
                background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-1 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             
             }
         
    `
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);

            }

            if (localStorage.getItem("WS_direct") == "Right") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
         @keyframes ball-run{
             0%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             40%{
                background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             80%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             100%{
                background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-2 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             
             }
         
    `
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);

            }


            if (localStorage.getItem("WS_direct") == "Up") {



                getkeyframes = (name) => {
                    var animation = {};
                    // 获取所有的style
                    var ss = document.styleSheets;
                    for (var i = 0; i < ss.length; ++i) {
                        const item = ss[i];
                        if (item.cssRules[0] && item.cssRules[0].name && item.cssRules[0].name === name) {
                            animation.cssRule = item.cssRules[0];
                            animation.styleSheet = ss[i];
                            animation.index = 0;

                        }
                    }
                    return animation;
                }

                const ballRunKeyframes_body = getkeyframes('ball-run');
                // deleteRule方法用来从当前样式表对象中删除指定的样式规则

                ballRunKeyframes_body.styleSheet.deleteRule(1);
                //重新定义ball从定位在(20,30)的位置运动到(400,500) 的位置
                const runkeyframesg_body = `
         @keyframes ball-run{
             0%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             40%{
                background-position-x:`+ (-0 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             80%{
                background-position-x:`+ (-1 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             100%{
                background-position-x:`+ (-2 * parseFloat(localStorage.getItem("WS_width"))) / 9 + `px;
                background-position-y:`+ (-3 * parseFloat(localStorage.getItem("WS_height"))) / 12 + `px;
             }
             
             
             }
         
    `
                // insertRule方法用来给当前样式表插入新的样式规则.

                ballRunKeyframes_body.styleSheet.insertRule(runkeyframesg_body, 1);

            }
            var uptInput = document.getElementById('btn_animeplay');

            uptInput.src = "./img/neko_animpause.png"

            localStorage.setItem("WS_anime", "Play");

            return false;
        }







    }, false);

    //------------------------------------------------------

    //眼睛

    fs.readdir('./www/assets/characters/eyes/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_eyes.push(file);

        });

        document.getElementById('eyes_chid').src = './img/None.png';

        var ul = document.getElementById('selert-eyes');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "eyes_0");
        li.setAttribute("class", "boty_igm");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addEyes(this)");
        li.setAttribute("imgname", "None.png");
        //設定 img 圖片地址
        img.src = './img/None.png';
        li.appendChild(img);
        ul.appendChild(li);


        for (var i = 0; i < asset_eyes.length; i++) {


            ul = document.getElementById('selert-eyes');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "eyes_" + i + 1);
            li.setAttribute("class", "boty_igm");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addEyes(this)");
            li.setAttribute("imgname", asset_eyes[i]);
            //設定 img 圖片地址
            img.src = './assets/characters/eyes/' + asset_eyes[i];
            li.appendChild(img);
            ul.appendChild(li);






        }


        document.getElementById('eyes_0').classList.add("fae");

        var img_url = './img/None.png';


        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印


            localStorage.setItem("WS_width", img.width);
            localStorage.setItem("WS_height", img.height);


            localStorage.setItem("RS_eyes", "None.png");



            var t = document.getElementById('eyes_img2'),
                s = document.getElementById('eyes_chid').getAttribute('src');

            t.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation:  ball-run 0.5s step-start 0s infinite alternate;-webkit-animation:  ball-run 0.5s step-start 0s infinite alternate;');

            var tz = document.getElementById('eyes_img1')

            tz.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 18 + 'px;width:' + img.width / 9 + 'px;background-position-x:-42px;background-position-y:' + (-0 * img.height) / 12 + 'px;');



            var tr_D = document.getElementById('D_eyes_img3')

            tr_D.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-D 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-D 0.5s step-start 0s infinite alternate;');

            var tr_L = document.getElementById('L_eyes_img3')

            tr_L.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-L 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-L 0.5s step-start 0s infinite alternate;');

            var tr_R = document.getElementById('R_eyes_img3')

            tr_R.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-R 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-R 0.5s step-start 0s infinite alternate;');

            var tr_U = document.getElementById('U_eyes_img3')

            tr_U.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-U 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-U 0.5s step-start 0s infinite alternate;');





        }


    });




    //眉毛

    fs.readdir('./www/assets/characters/eyebrow/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_eyebrow.push(file);

        });

        document.getElementById('eyebrow_chid').src = './img/None.png';




        var ul = document.getElementById('selert-eyebrow');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "eyebrow_0");
        li.setAttribute("class", "boty_igm");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addEyebrow(this)");
        li.setAttribute("imgname", "None.png");
        //設定 img 圖片地址
        img.src = './img/None.png';
        li.appendChild(img);
        ul.appendChild(li);




        for (var i = 0; i < asset_eyebrow.length; i++) {


            ul = document.getElementById('selert-eyebrow');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "eyebrow_" + i + 1);
            li.setAttribute("class", "boty_igm");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addEyebrow(this)");
            li.setAttribute("imgname", asset_eyebrow[i]);
            //設定 img 圖片地址
            img.src = './assets/characters/eyebrow/' + asset_eyebrow[i];
            li.appendChild(img);
            ul.appendChild(li);






        }


        document.getElementById('eyebrow_0').classList.add("fae");

        var img_url = './img/None.png';


        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印


            localStorage.setItem("WS_width", img.width);
            localStorage.setItem("WS_height", img.height);


            localStorage.setItem("RS_eyebrow", "None.png");



            var t = document.getElementById('eyebrow_img2'),
                s = document.getElementById('eyebrow_chid').getAttribute('src');

            t.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation:  ball-run 0.5s step-start 0s infinite alternate;-webkit-animation:  ball-run 0.5s step-start 0s infinite alternate;');

            var tz = document.getElementById('eyebrow_img1')

            tz.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 18 + 'px;width:' + img.width / 9 + 'px;background-position-x:-42px;background-position-y:' + (-0 * img.height) / 12 + 'px;');



            var tr_D = document.getElementById('D_eyebrow_img3')

            tr_D.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-D 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-D 0.5s step-start 0s infinite alternate;');

            var tr_L = document.getElementById('L_eyebrow_img3')

            tr_L.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-L 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-L 0.5s step-start 0s infinite alternate;');

            var tr_R = document.getElementById('R_eyebrow_img3')

            tr_R.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-R 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-R 0.5s step-start 0s infinite alternate;');

            var tr_U = document.getElementById('U_eyebrow_img3')

            tr_U.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-U 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-U 0.5s step-start 0s infinite alternate;');





        }


    });








    //髮型

    fs.readdir('./www/assets/characters/hairs/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_hairs.push(file);

        });



        document.getElementById('hairs_chid').src = './img/None.png';


        var ul = document.getElementById('selert-hairs');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");


        li.setAttribute("id", "hairs_0");
        li.setAttribute("class", "boty_igm");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addHairs(this)");
        li.setAttribute("imgname", "None.png");
        //設定 img 圖片地址
        img.src = './img/None.png';
        li.appendChild(img);
        ul.appendChild(li);



        for (var i = 0; i < asset_hairs.length; i++) {

            ul = document.getElementById('selert-hairs');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");

            //設定 img 屬性，如 id
            li.setAttribute("id", "hairs_" + i + 1);
            li.setAttribute("class", "boty_igm");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addHairs(this)");
            li.setAttribute("imgname", asset_hairs[i]);
            //設定 img 圖片地址
            img.src = './assets/characters/hairs/' + asset_hairs[i];
            li.appendChild(img);
            ul.appendChild(li);






        }


        document.getElementById('hairs_0').classList.add("fae");

        var img_url = './img/None.png';


        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印


            localStorage.setItem("WS_width", img.width);
            localStorage.setItem("WS_height", img.height);


            localStorage.setItem("RS_hairs", "None.png");



            var t = document.getElementById('hairs_img2'),
                s = document.getElementById('hairs_chid').getAttribute('src');

            t.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation:  ball-run 0.5s step-start 0s infinite alternate;-webkit-animation:  ball-run 0.5s step-start 0s infinite alternate;');

            var tz = document.getElementById('hairs_img1')

            tz.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 18 + 'px;width:' + img.width / 9 + 'px;background-position-x:-42px;background-position-y:' + (-0 * img.height) / 12 + 'px;');



            var tr_D = document.getElementById('D_hairs_img3')

            tr_D.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-D 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-D 0.5s step-start 0s infinite alternate;');

            var tr_L = document.getElementById('L_hairs_img3')

            tr_L.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-L 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-L 0.5s step-start 0s infinite alternate;');

            var tr_R = document.getElementById('R_hairs_img3')

            tr_R.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-R 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-R 0.5s step-start 0s infinite alternate;');

            var tr_U = document.getElementById('U_hairs_img3')

            tr_U.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-U 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-U 0.5s step-start 0s infinite alternate;');




        }


    });


    //眼鏡

    fs.readdir('./www/assets/characters/glasses/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_glasses.push(file);

        });

        document.getElementById('glasses_chid').src = './img/None.png';

        var ul = document.getElementById('selert-glasses');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");

        li.setAttribute("id", "glasses_0");
        li.setAttribute("class", "boty_igm");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addGlasses(this)");
        li.setAttribute("imgname", "None.png");
        //設定 img 圖片地址
        img.src = './img/None.png';
        li.appendChild(img);
        ul.appendChild(li);


        for (var i = 0; i < asset_glasses.length; i++) {


            ul = document.getElementById('selert-glasses');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "glasses_" + i + 1);
            li.setAttribute("class", "boty_igm");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addGlasses(this)");
            li.setAttribute("imgname", asset_glasses[i]);
            //設定 img 圖片地址
            img.src = './assets/characters/glasses/' + asset_glasses[i];
            li.appendChild(img);
            ul.appendChild(li);






        }


        document.getElementById('glasses_0').classList.add("fae");

        var img_url = './img/None.png';


        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印


            localStorage.setItem("WS_width", img.width);
            localStorage.setItem("WS_height", img.height);


            localStorage.setItem("RS_glasses", "None.png");



            var t = document.getElementById('glasses_img2'),
                s = document.getElementById('glasses_chid').getAttribute('src');

            t.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation:  ball-run 0.5s step-start 0s infinite alternate;-webkit-animation:  ball-run 0.5s step-start 0s infinite alternate;');

            var tz = document.getElementById('glasses_img1')

            tz.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 18 + 'px;width:' + img.width / 9 + 'px;background-position-x:-42px;background-position-y:' + (-0 * img.height) / 12 + 'px;');



            var tr_D = document.getElementById('D_glasses_img3')

            tr_D.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-D 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-D 0.5s step-start 0s infinite alternate;');

            var tr_L = document.getElementById('L_glasses_img3')

            tr_L.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-L 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-L 0.5s step-start 0s infinite alternate;');

            var tr_R = document.getElementById('R_glasses_img3')

            tr_R.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-R 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-R 0.5s step-start 0s infinite alternate;');

            var tr_U = document.getElementById('U_glasses_img3')

            tr_U.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-U 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-U 0.5s step-start 0s infinite alternate;');





        }


    });



    //嘴巴

    fs.readdir('./www/assets/characters/mouth/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_mouth.push(file);

        });

        document.getElementById('mouth_chid').src = './img/None.png';

        var ul = document.getElementById('selert-mouth');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "mouth_0");
        li.setAttribute("class", "boty_igm");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addMouth(this)");
        li.setAttribute("imgname", "None.png");
        //設定 img 圖片地址
        img.src = './img/None.png';
        li.appendChild(img);
        ul.appendChild(li);


        for (var i = 0; i < asset_mouth.length; i++) {


            ul = document.getElementById('selert-mouth');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "mouth_" + i + 1);
            li.setAttribute("class", "boty_igm");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addMouth(this)");
            li.setAttribute("imgname", asset_mouth[i]);
            //設定 img 圖片地址
            img.src = './assets/characters/mouth/' + asset_mouth[i];
            li.appendChild(img);
            ul.appendChild(li);






        }



        document.getElementById('mouth_0').classList.add("fae");

        var img_url = './img/None.png';


        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印


            localStorage.setItem("WS_width", img.width);
            localStorage.setItem("WS_height", img.height);


            localStorage.setItem("RS_mouth", "None.png");



            var t = document.getElementById('mouth_img2'),
                s = document.getElementById('mouth_chid').getAttribute('src');

            t.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation:  ball-run 0.5s step-start 0s infinite alternate;-webkit-animation:  ball-run 0.5s step-start 0s infinite alternate;');

            var tz = document.getElementById('mouth_img1')

            tz.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 18 + 'px;width:' + img.width / 9 + 'px;background-position-x:-42px;background-position-y:' + (-0 * img.height) / 12 + 'px;');



            var tr_D = document.getElementById('D_mouth_img3')

            tr_D.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-D 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-D 0.5s step-start 0s infinite alternate;');

            var tr_L = document.getElementById('L_mouth_img3')

            tr_L.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-L 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-L 0.5s step-start 0s infinite alternate;');

            var tr_R = document.getElementById('R_mouth_img3')

            tr_R.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-R 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-R 0.5s step-start 0s infinite alternate;');

            var tr_U = document.getElementById('U_mouth_img3')

            tr_U.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-U 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-U 0.5s step-start 0s infinite alternate;');





        }


    });


    //衣服下半

    fs.readdir('./www/assets/characters/lower_clothes/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_lower_clothes.push(file);

        });

        document.getElementById('lower_clothes_chid').src = './img/None.png';


        var ul = document.getElementById('selert-lower_clothes');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "lower_clothes_0");
        li.setAttribute("class", "boty_igm");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addLower_Clothes(this)");
        li.setAttribute("imgname", "None.png");
        //設定 img 圖片地址
        img.src = './img/None.png';
        li.appendChild(img);
        ul.appendChild(li);

        for (var i = 0; i < asset_lower_clothes.length; i++) {


            ul = document.getElementById('selert-lower_clothes');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "lower_clothes_" + i + 1);
            li.setAttribute("class", "boty_igm");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addLower_Clothes(this)");
            li.setAttribute("imgname", asset_lower_clothes[i]);
            //設定 img 圖片地址
            img.src = './assets/characters/lower_clothes/' + asset_lower_clothes[i];
            li.appendChild(img);
            ul.appendChild(li);






        }


        document.getElementById('lower_clothes_0').classList.add("fae");


        var img_url = './img/None.png';


        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印


            localStorage.setItem("WS_width", img.width);
            localStorage.setItem("WS_height", img.height);


            localStorage.setItem("RS_lower_clothes", "None.png");



            var t = document.getElementById('lower_clothes_img2'),
                s = document.getElementById('lower_clothes_chid').getAttribute('src');

            t.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation:  ball-run 0.5s step-start 0s infinite alternate;-webkit-animation:  ball-run 0.5s step-start 0s infinite alternate;');

            var tz = document.getElementById('lower_clothes_img1')

            tz.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 18 + 'px;width:' + img.width / 9 + 'px;background-position-x:-42px;background-position-y:' + (-0 * img.height) / 12 + 'px;');



            var tr_D = document.getElementById('D_lower_clothes_img3')

            tr_D.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-D 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-D 0.5s step-start 0s infinite alternate;');

            var tr_L = document.getElementById('L_lower_clothes_img3')

            tr_L.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-L 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-L 0.5s step-start 0s infinite alternate;');

            var tr_R = document.getElementById('R_lower_clothes_img3')

            tr_R.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-R 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-R 0.5s step-start 0s infinite alternate;');

            var tr_U = document.getElementById('U_lower_clothes_img3')

            tr_U.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-U 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-U 0.5s step-start 0s infinite alternate;');





        }


    });


    //衣服上半

    fs.readdir('./www/assets/characters/top_clothes/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_top_clothes.push(file);

        });

        document.getElementById('top_clothes_chid').src = './img/None.png';


        var ul = document.getElementById('selert-top_clothes');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "top_clothes_0");
        li.setAttribute("class", "boty_igm");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addTop_Clothes(this)");
        li.setAttribute("imgname", "None.png");
        //設定 img 圖片地址
        img.src = './img/None.png';
        li.appendChild(img);
        ul.appendChild(li);


        for (var i = 0; i < asset_top_clothes.length; i++) {


            ul = document.getElementById('selert-top_clothes');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "top_clothes_" + i + 1);
            li.setAttribute("class", "boty_igm");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addTop_Clothes(this)");
            li.setAttribute("imgname", asset_top_clothes[i]);
            //設定 img 圖片地址
            img.src = './assets/characters/top_clothes/' + asset_top_clothes[i];
            li.appendChild(img);
            ul.appendChild(li);






        }


        document.getElementById('top_clothes_0').classList.add("fae");


        var img_url = './img/None.png';


        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印


            localStorage.setItem("WS_width", img.width);
            localStorage.setItem("WS_height", img.height);


            localStorage.setItem("RS_top_clothes", "None.png");



            var t = document.getElementById('top_clothes_img2'),
                s = document.getElementById('top_clothes_chid').getAttribute('src');

            t.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation:  ball-run 0.5s step-start 0s infinite alternate;-webkit-animation:  ball-run 0.5s step-start 0s infinite alternate;');

            var tz = document.getElementById('top_clothes_img1')

            tz.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 18 + 'px;width:' + img.width / 9 + 'px;background-position-x:-42px;background-position-y:' + (-0 * img.height) / 12 + 'px;');



            var tr_D = document.getElementById('D_top_clothes_img3')

            tr_D.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-D 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-D 0.5s step-start 0s infinite alternate;');

            var tr_L = document.getElementById('L_top_clothes_img3')

            tr_L.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-L 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-L 0.5s step-start 0s infinite alternate;');

            var tr_R = document.getElementById('R_top_clothes_img3')

            tr_R.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-R 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-R 0.5s step-start 0s infinite alternate;');

            var tr_U = document.getElementById('U_top_clothes_img3')

            tr_U.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-U 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-U 0.5s step-start 0s infinite alternate;');





        }


    });



    //帽子

    fs.readdir('./www/assets/characters/hats/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_hats.push(file);

        });

        document.getElementById('hats_chid').src = './img/None.png';


        var ul = document.getElementById('selert-hats');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "hats_0");
        li.setAttribute("class", "boty_igm");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addHats(this)");
        li.setAttribute("imgname", "None.png");
        //設定 img 圖片地址
        img.src = './img/None.png';
        li.appendChild(img);
        ul.appendChild(li);



        for (var i = 0; i < asset_hats.length; i++) {


            ul = document.getElementById('selert-hats');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "hats_" + i + 1);
            li.setAttribute("class", "boty_igm");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addHats(this)");
            li.setAttribute("imgname", asset_hats[i]);
            //設定 img 圖片地址
            img.src = './assets/characters/hats/' + asset_hats[i];
            li.appendChild(img);
            ul.appendChild(li);






        }


        document.getElementById('hats_0').classList.add("fae");


        var img_url = './img/None.png';


        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印


            localStorage.setItem("WS_width", img.width);
            localStorage.setItem("WS_height", img.height);


            localStorage.setItem("RS_hats", "None.png");



            var t = document.getElementById('hats_img2'),
                s = document.getElementById('hats_chid').getAttribute('src');

            t.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation:  ball-run 0.5s step-start 0s infinite alternate;-webkit-animation:  ball-run 0.5s step-start 0s infinite alternate;');

            var tz = document.getElementById('hats_img1')

            tz.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 18 + 'px;width:' + img.width / 9 + 'px;background-position-x:-42px;background-position-y:' + (-0 * img.height) / 12 + 'px;');



            var tr_D = document.getElementById('D_hats_img3')

            tr_D.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-D 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-D 0.5s step-start 0s infinite alternate;');

            var tr_L = document.getElementById('L_hats_img3')

            tr_L.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-L 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-L 0.5s step-start 0s infinite alternate;');

            var tr_R = document.getElementById('R_hats_img3')

            tr_R.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-R 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-R 0.5s step-start 0s infinite alternate;');

            var tr_U = document.getElementById('U_hats_img3')

            tr_U.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-U 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-U 0.5s step-start 0s infinite alternate;');





        }


    });


    //飾品1

    fs.readdir('./www/assets/characters/accessories_01/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_accessories_01.push(file);

        });

        document.getElementById('accessories_01_chid').src = './img/None.png';

        var ul = document.getElementById('selert-accessories_01');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "accessories_01_0");
        li.setAttribute("class", "boty_igm");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addAccessories_01(this)");
        li.setAttribute("imgname", "None.png");
        //設定 img 圖片地址
        img.src = './img/None.png';
        li.appendChild(img);
        ul.appendChild(li);


        for (var i = 0; i < asset_accessories_01.length; i++) {


            ul = document.getElementById('selert-accessories_01');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "accessories_01_" + i + 1);
            li.setAttribute("class", "boty_igm");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addAccessories_01(this)");
            li.setAttribute("imgname", asset_accessories_01[i]);
            //設定 img 圖片地址
            img.src = './assets/characters/accessories_01/' + asset_accessories_01[i];
            li.appendChild(img);
            ul.appendChild(li);






        }


        document.getElementById('accessories_01_0').classList.add("fae");


        var img_url = './img/None.png';


        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印


            localStorage.setItem("WS_width", img.width);
            localStorage.setItem("WS_height", img.height);


            localStorage.setItem("RS_accessories_01", "None.png");



            var t = document.getElementById('accessories_01_img2'),
                s = document.getElementById('accessories_01_chid').getAttribute('src');

            t.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation:  ball-run 0.5s step-start 0s infinite alternate;-webkit-animation:  ball-run 0.5s step-start 0s infinite alternate;');

            var tz = document.getElementById('accessories_01_img1')

            tz.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 18 + 'px;width:' + img.width / 9 + 'px;background-position-x:-42px;background-position-y:' + (-0 * img.height) / 12 + 'px;');



            var tr_D = document.getElementById('D_accessories_01_img3')

            tr_D.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-D 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-D 0.5s step-start 0s infinite alternate;');

            var tr_L = document.getElementById('L_accessories_01_img3')

            tr_L.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-L 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-L 0.5s step-start 0s infinite alternate;');

            var tr_R = document.getElementById('R_accessories_01_img3')

            tr_R.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-R 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-R 0.5s step-start 0s infinite alternate;');

            var tr_U = document.getElementById('U_accessories_01_img3')

            tr_U.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-U 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-U 0.5s step-start 0s infinite alternate;');





        }


    });



    //飾品2

    fs.readdir('./www/assets/characters/accessories_02/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_accessories_02.push(file);

        });

        document.getElementById('accessories_02_chid').src = './img/None.png';

        var ul = document.getElementById('selert-accessories_02');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "accessories_02_0");
        li.setAttribute("class", "boty_igm");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addAccessories_02(this)");
        li.setAttribute("imgname", "None.png");
        //設定 img 圖片地址
        img.src = './img/None.png';
        li.appendChild(img);
        ul.appendChild(li);


        for (var i = 0; i < asset_accessories_02.length; i++) {


            ul = document.getElementById('selert-accessories_02');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "accessories_02_" + i + 1);
            li.setAttribute("class", "boty_igm");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addAccessories_02(this)");
            li.setAttribute("imgname", asset_accessories_02[i]);
            //設定 img 圖片地址
            img.src = './assets/characters/accessories_02/' + asset_accessories_02[i];
            li.appendChild(img);
            ul.appendChild(li);






        }


        document.getElementById('accessories_02_0').classList.add("fae");


        var img_url = './img/None.png';


        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印


            localStorage.setItem("WS_width", img.width);
            localStorage.setItem("WS_height", img.height);


            localStorage.setItem("RS_accessories_02", "None.png");



            var t = document.getElementById('accessories_02_img2'),
                s = document.getElementById('accessories_02_chid').getAttribute('src');

            t.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation:  ball-run 0.5s step-start 0s infinite alternate;-webkit-animation:  ball-run 0.5s step-start 0s infinite alternate;');

            var tz = document.getElementById('accessories_02_img1')

            tz.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 18 + 'px;width:' + img.width / 9 + 'px;background-position-x:-42px;background-position-y:' + (-0 * img.height) / 12 + 'px;');



            var tr_D = document.getElementById('D_accessories_02_img3')

            tr_D.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-D 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-D 0.5s step-start 0s infinite alternate;');

            var tr_L = document.getElementById('L_accessories_02_img3')

            tr_L.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-L 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-L 0.5s step-start 0s infinite alternate;');

            var tr_R = document.getElementById('R_accessories_02_img3')

            tr_R.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-R 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-R 0.5s step-start 0s infinite alternate;');

            var tr_U = document.getElementById('U_accessories_02_img3')

            tr_U.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-U 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-U 0.5s step-start 0s infinite alternate;');





        }


    });



    //其他

    fs.readdir('./www/assets/characters/others/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach(function (file) {
            asset_others.push(file);

        });

        document.getElementById('others_chid').src = './img/None.png';


        var ul = document.getElementById('selert-others');
        //新增 li
        var li = document.createElement("a");
        //新增 img
        var img = document.createElement("img");
        //設定 img 屬性，如 id
        li.setAttribute("id", "others_0");
        li.setAttribute("class", "boty_igm");
        li.setAttribute("href", "javascript:");
        li.setAttribute("onclick", "addOthers(this)");
        li.setAttribute("imgname", "None.png");
        //設定 img 圖片地址
        img.src = './img/None.png';
        li.appendChild(img);
        ul.appendChild(li);

        for (var i = 0; i < asset_others.length; i++) {


            ul = document.getElementById('selert-others');
            //新增 li
            li = document.createElement("a");
            //新增 img
            img = document.createElement("img");
            //設定 img 屬性，如 id
            li.setAttribute("id", "others_" + i + 1);
            li.setAttribute("class", "boty_igm");
            li.setAttribute("href", "javascript:");
            li.setAttribute("onclick", "addOthers(this)");
            li.setAttribute("imgname", asset_others[i]);
            //設定 img 圖片地址
            img.src = './assets/characters/others/' + asset_others[i];
            li.appendChild(img);
            ul.appendChild(li);






        }


        document.getElementById('others_0').classList.add("fae");


        var img_url = './img/None.png';


        var img = new Image()

        // 改变图片的src
        img.src = img_url

        // 加载完成执行
        img.onload = function () {
            // 打印


            localStorage.setItem("WS_width", img.width);
            localStorage.setItem("WS_height", img.height);


            localStorage.setItem("RS_others", "None.png");



            var t = document.getElementById('others_img2'),
                s = document.getElementById('others_chid').getAttribute('src');

            t.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation:  ball-run 0.5s step-start 0s infinite alternate;-webkit-animation:  ball-run 0.5s step-start 0s infinite alternate;');

            var tz = document.getElementById('others_img1')

            tz.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 18 + 'px;width:' + img.width / 9 + 'px;background-position-x:-42px;background-position-y:' + (-0 * img.height) / 12 + 'px;');




            var tr_D = document.getElementById('D_others_img3')

            tr_D.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-D 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-D 0.5s step-start 0s infinite alternate;');

            var tr_L = document.getElementById('L_others_img3')

            tr_L.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-L 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-L 0.5s step-start 0s infinite alternate;');

            var tr_R = document.getElementById('R_others_img3')

            tr_R.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-R 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-R 0.5s step-start 0s infinite alternate;');

            var tr_U = document.getElementById('U_others_img3')

            tr_U.setAttribute('style', 'background-image:url(' + s + ');background-size:' + img.width / 3 + 'px;height:' + img.height / 12 + 'px;width:' + img.width / 9 + 'px;animation: ball-run-U 0.5s step-start 0s infinite alternate;-webkit-animation: ball-run-U 0.5s step-start 0s infinite alternate;');




        }


    });


}); 